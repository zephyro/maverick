
ymaps.ready(init);

function init () {

    var ContactMap = new ymaps.Map("contact-map", {
        center: [48.7200,44.5028],
        zoom: 14,
        controls: ['smallMapDefaultSet']
    });

    myPlacemark = new ymaps.Placemark([48.7209,44.5032], {
        hintContent: ''
    }, {
        iconLayout: 'default#image',
        iconImageHref: 'img/placemark2.png',
        iconImageSize: [51, 64],
        iconImageOffset: [-26, -65]
    });

    ContactMap.behaviors.disable('scrollZoom');
    ContactMap.behaviors.disable('multiTouch');
    ContactMap.geoObjects.add(myPlacemark);
}
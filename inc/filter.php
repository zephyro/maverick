<a class="filter-header btn-switch svg-responsive" data-target=".panel-switch-inner">
    <img src="img/btn-toggle.svg" alt="">
</a>

<div class="panel-switch-inner filter-inner">
    <div class="filter panel-switch">

        <div class="filter-top">
            <h4>Фильтр</h4>
            <div class="filter-sort">
                <span class="filter-sort-head">Сортировать</span>
                <div class="filter-sort-value">
                    <div class="wrapper-dropdown" id="dd2">
                        <span class="dropdown-name">по убыванию цены</span>
                        <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
                        <div class="list-dropdown">
                            <span>сортировать по</span>
                            <ul>
                                <li><a href="#">по убыванию цены</a></li>
                                <li><a href="#">по возрастанию цены</a></li>
                                <li><a href="#">по алфавиту</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-checkbox">
                <label>
                    <span class="icr-text">Показывать<br/>характеристики</span>
                    <input type="checkbox" name="view" value="1">
                </label>
            </div>
            <span class="filter-close panel-switch-hide"></span>
        </div>

        <form class="form">
            <!-- Сезон -->
            <div class="filter-item">

                <div class="filter-title">
                    <span class="filter-name">Сезон</span>
                </div>

                <ul class="filter-body">
                    <li>
                        <label class="checked">
                            <span class="icr-text">2015</span>
                            <input type="checkbox" name="f1" value="1" checked>
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">2016</span>
                            <input type="checkbox" name="f1" value="2">
                        </label>
                    </li>
                </ul>

            </div><!-- -->

            <!-- Тип велосипеда -->
            <div class="filter-item">

                <div class="filter-title">
                    <span class="filter-name">Тип велосипеда <i class="btn-tooltip" rel="tooltip" title="цена в вашем городе может быть выше">?</i></span></span>
                </div>

                <ul class="filter-body">
                    <li>
                        <label class="checked">
                            <span class="icr-text">Горный хардтейл</span>
                            <input type="checkbox" name="f2" value="1" checked>
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">Горный двухподвес</span>
                            <input type="checkbox" name="f2" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">Дорожный</span>
                            <input type="checkbox" name="f2" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">Подростковый</span>
                            <input type="checkbox" name="f2" value="2">
                        </label>
                    </li>
                </ul>
            </div><!-- -->

            <!-- Возраст -->
            <div class="filter-item">

                <div class="filter-title">
                    <span class="filter-name">Возраст <i class="btn-tooltip" rel="tooltip" title="цена в вашем городе может быть выше">?</i></span></span>
                </div>

                <ul class="filter-body">
                    <li>
                        <label class="checked">
                            <span class="icr-text">Детский</span>
                            <input type="checkbox" name="f3" value="1" checked>
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">Взрослый</span>
                            <input type="checkbox" name="f3" value="2">
                        </label>
                    </li>
                </ul>
            </div><!-- -->

            <!-- Пол -->
            <div class="filter-item"><!-- -->

                <div class="filter-title">
                    <span class="filter-name">Пол</span>
                </div>

                <ul class="filter-body">
                    <li>
                        <label class="checked">
                            <span class="icr-text">Мужской</span>
                            <input type="checkbox" name="f4" value="1" checked>
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">Женский</span>
                            <input type="checkbox" name="f4" value="2">
                        </label>
                    </li>
                </ul>

            </div><!-- -->

            <!-- Цвет велосипеда -->
            <div class="filter-item">

                <div class="filter-title">
                    <span class="filter-name">Цвет велосипеда</span>
                </div>

                <ul class="filter-body colors">
                    <li>
                        <label class="checked">
                                        <span class="icr-text">
                                            <img src="img/filter-red.png">
                                        </span>
                            <input type="checkbox" name="f5" value="1" checked>
                        </label>
                    </li>
                    <li>
                        <label>
                                        <span class="icr-text">
                                            <img src="img/filter-yellow.png">
                                        </span>
                            <input type="checkbox" name="f5" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                                        <span class="icr-text">
                                            <img src="img/filter-green.png">
                                        </span>
                            <input type="checkbox" name="f5" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                                        <span class="icr-text">
                                            <img src="img/filter-blue.png">
                                        </span>
                            <input type="checkbox" name="f5" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                                        <span class="icr-text">
                                            <img src="img/filter-black.png">
                                        </span>
                            <input type="checkbox" name="f5" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                                        <span class="icr-text">
                                            <img src="img/filter-white.png">
                                        </span>
                            <input type="checkbox" name="f5" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                                        <span class="icr-text">
                                            <img src="img/filter-white-gray.png">
                                        </span>
                            <input type="checkbox" name="f5" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                                        <span class="icr-text">
                                            <img src="img/filter-yellow-brown.png">
                                        </span>
                            <input type="checkbox" name="f5" value="2">
                        </label>
                    </li>
                </ul>

            </div><!-- -->

            <!-- Материал рамы -->
            <div class="filter-item"><!-- -->

                <div class="filter-title">
                    <span class="filter-name">Материал рамы</span>
                </div>

                <ul class="filter-body">
                    <li>
                        <label class="checked">
                            <span class="icr-text">Металл</span>
                            <input type="checkbox" name="f6" value="1" checked>
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">Пластик</span>
                            <input type="checkbox" name="f6" value="2">
                        </label>
                    </li>
                </ul>

            </div><!-- -->

            <!-- Количество скоростей -->
            <div class="filter-item"><!-- -->

                <div class="filter-title">
                    <span class="filter-name">Количество скоростей</span>
                </div>

                <ul class="filter-body">
                    <li>
                        <label class="checked">
                            <span class="icr-text">3</span>
                            <input type="checkbox" name="f7" value="1" checked>
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">5</span>
                            <input type="checkbox" name="f7" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">7</span>
                            <input type="checkbox" name="f6" value="2">
                        </label>
                    </li>
                </ul>

            </div><!-- -->

            <!-- Размер колес -->
            <div class="filter-item"><!-- -->

                <div class="filter-title">
                    <span class="filter-name">Размер колес</span>
                </div>

                <ul class="filter-body">
                    <li>
                        <label class="checked">
                            <span class="icr-text">20"</span>
                            <input type="checkbox" name="f8" value="1" checked>
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">26""</span>
                            <input type="checkbox" name="f8" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">30""</span>
                            <input type="checkbox" name="f8" value="2">
                        </label>
                    </li>
                </ul>

            </div><!-- -->

            <!-- Тормоза -->
            <div class="filter-item"><!-- -->

                <div class="filter-title">
                    <span class="filter-name">Тормоза</span>
                </div>

                <ul class="filter-body">
                    <li>
                        <label class="checked">
                            <span class="icr-text">есть</span>
                            <input type="checkbox" name="f9" value="1" checked>
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">нет</span>
                            <input type="checkbox" name="f9" value="2">
                        </label>
                    </li>
                </ul>

            </div><!-- -->

            <!-- Тип вилки -->
            <div class="filter-item"><!-- -->

                <div class="filter-title">
                    <span class="filter-name">Тип вилки</span>
                </div>

                <ul class="filter-body">
                    <li>
                        <label class="checked">
                            <span class="icr-text">тип 1</span>
                            <input type="checkbox" name="f10" value="1" checked>
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">тип 2</span>
                            <input type="checkbox" name="f10" value="2">
                        </label>
                    </li>
                </ul>

            </div><!-- -->

            <!-- Размер рамы -->
            <div class="filter-item"><!-- -->

                <div class="filter-title">
                    <span class="filter-name">Размер рамы</span>
                </div>

                <ul class="filter-body filter-size">
                    <li>
                        <label class="checked">
                            <span class="icr-text">21”</span>
                            <input type="checkbox" name="f11" value="1" checked>
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">20”</span>
                            <input type="checkbox" name="f11" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">19.5”</span>
                            <input type="checkbox" name="f11" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">19”</span>
                            <input type="checkbox" name="f11" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">17.5”</span>
                            <input type="checkbox" name="f11" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">17”</span>
                            <input type="checkbox" name="f11" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">16”</span>
                            <input type="checkbox" name="f11" value="2">
                        </label>
                    </li>
                    <li>
                        <label>
                            <span class="icr-text">14”</span>
                            <input type="checkbox" name="f11" value="2">
                        </label>
                    </li>
                </ul>

            </div><!-- -->

            <div class="filter-footer">
                <p class="text-center">
                    <button type="submit" class="btn">Подобрать</button>
                </p>
                <div class="text-center">
                    <button type="reset" class="btn-transparent">сбросить параметры <i class="fa fa-close"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
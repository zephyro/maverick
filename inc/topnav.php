<nav class="topnav">
    <div class="container">
        <div class="topnav-header clearfix">
            <a href="/" class="logo">
                <img src="img/logo.svg" alt="">
            </a>
            <span class="navbar-toggle" data-target=".navbar"><i class="fa fa-navicon"></i></span>
        </div>

        <!-- Navigation -->

        <div class="navbar clearfix">
            <ul class="navbar-header clearfix">
                <li class="navbar-back"><span></span></li>
                <li class="navbar-name">Меню</li>
                <li class="navbar-close"></li>
            </ul>
            <ul class="nav clearfix">
                <li class="active parents-nav">
                    <a href="#">Каталог</a>
                    <div class="subnav subnav-bike">
                        <div class="container">
                            <ul class="clearfix">
                                <li>
                                    <a href="#">
                                        <div class="nav-bike">
                                            <img src="img/nav_bike01.png" alt="">
                                        </div>
                                        <div class="nav-bike-title">Горные хардтейлы</div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="nav-bike">
                                            <img src="img/nav_bike02.png" alt="" class="img-responsive">
                                        </div>
                                        <div class="nav-bike-title">Горные двухподвесы</div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="nav-bike">
                                            <img src="img/nav_bike03.png" alt="" class="img-responsive">
                                        </div>
                                        <div class="nav-bike-title">Дорожные</div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="nav-bike">
                                            <img src="img/nav_bike04.png" alt="" class="img-responsive">
                                        </div>
                                        <div class="nav-bike-title">Подростковые</div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li><a href="#">Дилерам</a></li>
                <li><a href="#">Новости</a></li>
                <li class="parents-nav dropdown">
                    <a href="#">О компании</a>
                    <ul class="subnav">
                        <li><a href="#">О нас</a></li>
                        <li><a href="#">Развитие компании</a></li>
                        <li><a href="#">Наша миссия</a></li>
                        <li><a href="#">Преимущества</a></li>
                        <li><a href="#">Наши партнеры</a></li>
                        <li><a href="#">Новости</a></li>
                    </ul>
                </li>
                <li><a href="#">Контакты</a></li>
                <li><a href="#" class="btn btn-sm">Где купить</a></li>
            </ul><!-- -->

            <div class="second-nav">
                <ul>
                    <li><a href="#">Каталог 2016</a></li>
                    <li><a href="#">Каталог 2015</a></li>
                    <li><a href="#">Вход дилерам</a></li>
                </ul>
                <div class="navbar-contact">г. Москва, <a href="tel:+7 (925) 118-30-70">+7 (925) 118-30-70</a></div>
                <a href="mailto:veloart@bk.ru">veloart@bk.ru</a>
            </div>
        </div>
    </div>
</nav>
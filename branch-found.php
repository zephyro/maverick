<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->


    <section class="branch pb50">
        <div class="container">
            <a class="btn-toggle btn-switch svg-responsive" data-target=".panel-switch-inner">
                <img src="img/btn-toggle.svg" alt="">
            </a>

            <div class="panel-switch-inner">
                <div class="map-filter panel-switch">
                    <div class="map-filter-content">
                        <span class="map-filter-hide panel-switch-hide"></span>
                        <div class="map-filter-search">
                            <form class="form">
                                <input type="text" class="form-search" placeholder="Искать по названию">
                            </form>
                        </div>
                        <div class="map-filter-result">
                            <div class="result-text">
                                <div class="clearfix mb50">
                                    <div class="form-checkbox">
                                        <label>
                                            <span class="icr-text">Интернет-магазины</span>
                                            <input type="checkbox" name="f2" value="2">
                                        </label>
                                    </div>
                                    <div class="form-checkbox">
                                        <label>
                                            <span class="icr-text">Магазины</span>
                                            <input type="checkbox" name="f2" value="2">
                                        </label>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-md">Показать</button>
                            </div>
                            <div class="result-none">Мы не смогли найти<br/>такой регион</div>
                            <div class="result-list hide">
                                <ul>
                                    <li>Набережные Челны</li>
                                    <li>Нижневартовск</li>
                                    <li>Нижний Тагил</li>
                                    <li>Новокузнецк</li>
                                    <li>Новокуйбышевск</li>
                                    <li>Новочеркасск</li>
                                    <li>Ноябрьск</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="map-content">
                <div class="h1">Где купить<br/></div>
                <div class="map-list hidden-xs">
                    <div class="form-group">
                        <select class="form-control select-style" name="region">
                            <option>По всей России</option>
                            <option>Москва</option>
                            <option>Санкт-Петербург</option>
                            <option>Тамбов</option>
                            <option>Волгоград</option>
                            <option>Орел</option>
                        </select>
                    </div>
                    <div class="branch-type clearfix">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-checkbox">
                                    <label>
                                        <span class="icr-text">Интернет-магазины</span>
                                        <input type="checkbox" name="f2" value="2">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-checkbox">
                                    <label>
                                        <span class="icr-text">Магазины</span>
                                        <input type="checkbox" name="f2" value="2">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="wmap"></div>
        <div class="container">
            <ul class="branch-title">
                <li class="branch-title-legend">всего по России:</li>
                <li class="branch-title-shop"><span>9 магазинов</span></li>
                <li class="branch-title-online"><span>и 6 интернет-магазинов</span></li>
            </ul>

            <div class="branch-text">
                <h3>Дилеры в Перми</h3>

                <ul class="branch-result">

                    <li>
                        <ul class="branch-result-text">
                            <li class="branch-result-name">ИП Тенькофф</li>
                           <li>
                               <h5>Адрес</h5>
                               <p>ул. Третьего Взятия Бастилии, 17а, корп. 34, оф. 89</p>
                           </li>
                            <li>
                                <h5>Телефон</h5>
                                <p>(495) 678-90-00</p>
                            </li>
                            <li>
                                <h5>Сайт</h5>
                                <p> www.grecha-opt.ru</p>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <ul class="branch-result-text">
                            <li class="branch-result-name">ООО "ГлобалИнтернешнл"</li>
                            <li>
                                <h5>Адрес</h5>
                                <p>ул. Третьего Взятия Бастилии, 17а, корп. 34, оф. 89</p>
                            </li>
                            <li>
                                <h5>Телефон</h5>
                                <p>(495) 678-90-00</p>
                            </li>
                            <li>
                                <h5>Сайт</h5>
                                <p> www.grecha-opt.ru</p>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <ul class="branch-result-text">
                            <li class="branch-result-name">ИП Гречишников</li>
                            <li>
                                <h5>Адрес</h5>
                                <p>ул. Третьего Взятия Бастилии, 17а, корп. 34, оф. 89</p>
                            </li>
                            <li>
                                <h5>Телефон</h5>
                                <p>(495) 678-90-00</p>
                            </li>
                            <li>
                                <h5>Сайт</h5>
                                <p> www.grecha-opt.ru</p>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <ul class="branch-result-text">
                            <li class="branch-result-name">ООО «Риос-Спорт»</li>
                            <li>
                                <h5>Адрес</h5>
                                <p>ул. Третьего Взятия Бастилии, 17а, корп. 34, оф. 89</p>
                            </li>
                            <li>
                                <h5>Телефон</h5>
                                <p>(495) 678-90-00</p>
                            </li>
                            <li>
                                <h5>Сайт</h5>
                                <p> www.grecha-opt.ru</p>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <ul class="branch-result-text">
                            <li class="branch-result-name">«ТелоНаВело»</li>
                            <li>
                                <h5>Адрес</h5>
                                <p>ул. Третьего Взятия Бастилии, 17а, корп. 34, оф. 89</p>
                            </li>
                            <li>
                                <h5>Телефон</h5>
                                <p>(495) 678-90-00</p>
                            </li>
                            <li>
                                <h5>Сайт</h5>
                                <p> www.grecha-opt.ru</p>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </section>


    <!-- Footer Banner -->
    <?php include('inc/promo.php') ?>
    <!-- -->

    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->



    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    <script src="js/service.map.js"></script>

    </body>
</html>

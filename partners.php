<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

            <ul class="breadcrumbs">
                <li><a href="#">Главная</a></li>
            </ul>

            <h1>Наши партнеры</h1>

            <div class="partners">
                <div class="partners-body">

                    <div class="partners-row">

                        <div class="partners-item item1 active">
                            <div class="partners-logo">
                                <img src="images/partners_logo_01.png" alt="" class="img-responsive">
                            </div>
                            <h4>SHIMANO</h4>
                            <p>Компания, основанная в Японии в 1921 г. – мировой лидер в производстве высококачественных компонентов для велосипедов. Продукцию Shimano используют все мировые велобренды.</p>
                        </div>

                        <div class="partners-item item2">
                            <div class="partners-logo">
                                <img src="images/partners_logo_02.png" alt="" class="img-responsive">
                            </div>
                            <h4>Trinx</h4>
                            <p>Trinx – стратегический партнер Forward по производству рамных комплектов. Компания Trinx основана в 1992 году в Китае. Годовая мощность производства Trinx – 1,6 млн. стальных и алюминиевых рам, 1,4 млн. жестких и амортизационных вилок. Продукция Trinx поставляется в Европу, Юго-Восточную Азию, страны Ближнего Востока и в Африку.</p>
                        </div>

                        <div class="partners-item item3">
                            <div class="partners-logo">
                                <img src="images/partners_logo_03.png" alt="" class="img-responsive">
                            </div>
                            <h4>Sram</h4>
                            <p>Компания, основанная в 1987 г. в США – один из крупнейших в мире производителей комплектующих для велосипедов под брендами SRAM, RockShox, Truvativ, Avid и др.</p>
                        </div>

                    </div>

                    <div class="partners-row">

                        <div class="partners-item item4">
                            <div class="partners-logo">
                                <img src="images/partners_logo_01.png" alt="" class="img-responsive">
                            </div>
                            <h4>SHIMANO</h4>
                            <p>Компания – один из мировых лидеров по производству покрышек с 20 000 сотрудниками по всему миру. Продукция CST сертифицирована по стандарту ISO9001 и продается в 150 странах мира.</p>
                        </div>

                        <div class="partners-item item5">
                            <div class="partners-logo">
                                <img src="images/partners_logo_02.png" alt="" class="img-responsive">
                            </div>
                            <h4>Trinx</h4>
                            <p>Одна из старейших компаний-производителей изделий из резины в мире – ведет свою историю с 1908 г. Высококачественные велопокрышки Rubena сертифицированы по стандартам ISO9001</p>
                        </div>

                        <div class="partners-item item6">
                            <div class="partners-logo">
                                <img src="images/partners_logo_03.png" alt="" class="img-responsive">
                            </div>
                            <h4>Sram</h4>
                            <p>Один из крупнейших производителей велопокрышек в Китае. В компании работает более 10 000 человек на 6 заводах в 3 странах мира. Продукция Kenda поставляется в 150 стран.</p>
                        </div>

                    </div>

                    <div class="partners-row">

                        <div class="partners-item item7">
                            <div class="partners-logo">
                                <img src="images/partners_logo_01.png" alt="" class="img-responsive">
                            </div>
                            <h4>SHIMANO</h4>
                            <p>Компания, основанная в Японии в 1921 г. – мировой лидер в производстве высококачественных компонентов</p>
                        </div>

                        <div class="partners-item item8">
                            <div class="partners-logo">
                                <img src="images/partners_logo_02.png" alt="" class="img-responsive">
                            </div>
                            <h4>Trinx</h4>
                            <p>Trinx – стратегический партнер Forward по производству рамных комплектов. Компания Trinx основана в 1992 году в Китае. </p>
                        </div>
                    </div>

                </div>

                <ul class="partners-nav">
                    <li class="active"><a href="#" data-target=".item1"></a></li>
                    <li><a href="#" data-target=".item2"></a></li>
                    <li><a href="#" data-target=".item3"></a></li>
                    <li><a href="#" data-target=".item4"></a></li>
                    <li><a href="#" data-target=".item5"></a></li>
                    <li><a href="#" data-target=".item6"></a></li>
                    <li><a href="#" data-target=".item7"></a></li>
                    <li><a href="#" data-target=".item8"></a></li>
                </ul>

            </div>
        </div>
    </section>

    <!-- Footer Banner -->
    <?php include('inc/promo.php') ?>
    <!-- -->

    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->



    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    </body>
</html>

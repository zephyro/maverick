<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="#">Главная</a></li>
                <li><a href="#">Новости</a></li>
            </ul>
            <h1>BMX-трек в виде легких</h1>
            <div class="news news-single clearfix">
                <span class="news-toggle btn-switch" data-target=".panel-switch-inner">
                    <img src="img/btn-toggle.svg" alt="">
                </span>

                <div class="news-body">

                    <article class="post">

                        <p>В Китае, в Тайбэе, построили необычный трек в виде человеческих легких. Самая главная «фишка» – это меняющийся в зависимости от состава воздуха цвет. Разработчики инсталляции хотели привлечь внимание общественности к проблеме загрязнения окружающей среды и увеличении количества выхлопов из-за постоянно растущего числа автомобилей.</p>
                        <p>Испытатели провели эксперимент – показали, что при приближении проплывающего корабля, например, цвет легких становится оранжевым. Трек способен менять цвет от сине-зеленого до насыщенных оттенков красного. Когда мимо проезжает велосипедист, инсталляция приобретает насыщенный изумрудный цвет.</p>
                        <p>Создатели этого необычного трека верят в то, что их проект поможет людям задуматься о том, стоит ли покупать автомобиль вообще. Может, легче купить каждому члену семьи по велосипеду и сделать окружающую среду намного чище!</p>

                        <div class="post-image-slider">
                            <div class="post-image-item">
                                <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                            </div>
                            <div class="post-image-item">
                                <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                            </div>
                            <div class="post-image-item">
                                <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                            </div>
                            <div class="post-image-item">
                                <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                            </div>
                            <div class="post-image-item">
                                <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                            </div>
                            <div class="post-image-item">
                                <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                            </div>
                            <div class="post-image-item">
                                <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                            </div>
                            <div class="post-image-item">
                                <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                            </div>
                        </div>
                </div>

                <div class="news-side">

                    <div class="post-meta">
                        <span class="post-date">258 мая 2016</span>
                        <ul class="post-tags">
                            <li><a href="#">акция</a></li>
                            <li><a href="#">новинка</a></li>
                            <li><a href="#">снижение цены</a></li>
                        </ul>
                    </div>

                </div>
            </div>
            <a href="#" class="post-back">Вернуться к ленте новостей</a>
        </div>
    </section>

    <div class="news-block">
        <div class="container">
            <div class="h2">Еще новости</div>

            <div class="news-block-slider">

                <div class="news-block-item">
                    <a href="#" class="news-block-image">
                        <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                    </a>
                    <div class="news-block-date">20 мая 2016</div>
                    <h4><a href="#">Yike Bike – велосипед будущего</a></h4>
                </div>

                <div class="news-block-item">
                    <a href="#" class="news-block-image">
                        <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                    </a>
                    <div class="news-block-date">20 мая 2016</div>
                    <h4><a href="#">Yike Bike – велосипед будущего</a></h4>
                </div>

                <div class="news-block-item">
                    <a href="#" class="news-block-image">
                        <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                    </a>
                    <div class="news-block-date">20 мая 2016</div>
                    <h4><a href="#">Yike Bike – велосипед будущего</a></h4>
                </div>

                <div class="news-block-item">
                    <a href="#" class="news-block-image">
                        <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                    </a>
                    <div class="news-block-date">20 мая 2016</div>
                    <h4><a href="#">Yike Bike – велосипед будущего</a></h4>
                </div>

            </div>
        </div>
    </div>

    <!-- Footer Banner -->
    <?php include('inc/promo.php') ?>
    <!-- -->


    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->

    <!-- Modal -->
    <div class="hide">
        <div class="modal modal-sm" id="question">
            <div class="modal-header">Поддержка клиента</div>
            <div class="modal-body">
                <h3>Поддержка клиента</h3>
                <form class="form">
                    <div class="form-group">
                        <input type="text" name="" class="form-control" placeholder="Ваше имя">
                    </div>
                    <div class="form-group">
                        <input type="text" name="" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <textarea name="message" class="form-control" placeholder="Текст сообщения" rows="4"></textarea>
                    </div>
                    <div class="pb20"></div>
                    <button type="submit" class="btn btn-send">Отправить</button>
                </form>
            </div>
        </div>
    </div>
    <!-- -->


    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    </body>
</html>

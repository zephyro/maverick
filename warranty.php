<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

<body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->

    <header class="warranty-header">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="#">Главная</a></li>
            </ul>
            <div class="heading">Качество велосипедов MAVERICK подтверждено расширенной гарантией</div>
            <a href="#" class="btn">Ближайший сервисный центр</a>

            <ul class="warranty-advantage clearfix">
                <li class="item1">
                    <div class="warranty-value">3 года</div>
                    <div class="warranty-legend">срок службы изделия</div>
                </li>
                <li class="item2">
                    <div class="warranty-value">1 год</div>
                    <div class="warranty-legend">гарантия на раму</div>
                </li>
                <li class="item3">
                    <div class="warranty-value">6 месяцев</div>
                    <div class="warranty-legend">гарантия на навесное оборудование</div>
                </li>
            </ul>
        </div>
    </header>

    <!--  -->
    <section class="main">
        <div class="container">

            <div class="content">
                <p>Изготовитель установил срок службы изделия – 3 года при соблюдении всех рекомендаций по использованию, уходу и хранению велосипеда Maverick, которые указаны в Руководстве. </p>
                <p>Срок, в течение которого Покупатель имеет право на гарантийный ремонт рамы – 1 год, навесного оборудования – 6 месяцев. Исчисление гарантийного срока начинается со дня продажи (передачи) велосипеда Покупателю</p>
                <br/>  <br/>  <br/>
                <h2>Условия предоставления гарантии</h2>
                <p>Срок гарантии начинает исчисляется соz дня продажи (передачи) велосипеда Покупателю. На протяжении гарантийного срока изготовитель обязан выполнять устранение поломок, дефектов, которые возникли не по вине Покупателя и не в результате действий сторонних лиц или непреодолимых обстоятельств. Гарантийный ремонт производится без взимания оплаты с Покупателя.</p>
                <p>Гарантийные обязательства распространяются на любые дефекты в материалах, комплектующих и узлах велосипеда, которые возникли по вине изготовителя. Они заключаются в ремонте, который выполняется без оплаты Покупателем. </p>
                <p>Право выполнять гарантийный ремонт имеют исключительно мастера авторизованных сервисных центров, которые имеют Сертификат установленной формы.</p>
                <p>При обнаружении каких-либо дефектов и поломок Покупатель своими силами должен доставить изделие к Продавцу или в сервисный центр. Расходы на доставку неисправного велосипеда к месту ремонта возмещению не подлежат. На ремонт принимаются чистые изделия в комплектном состоянии. </p>
                <p>Паспорт велосипеда должен быть заполнен полностью и легко читаться. В документе указывается модель, даты сборки и продажи, серийные номера комплектующих. Все указанные данные подтверждаются подписью Покупателя и печатью торговой точки.</p>
            </div>
            <ul class="links clearfix">
                <li><a href="#">Покупатель обязуется</a></li>
                <li><a href="#">Гарантийные обязательства изготовителя не распространяются на..</a></li>
                <li><a href="#">Причины снятия велосипеда Maverick с гарантийного обслуживания</a></li>
            </ul>
            <h2 class="text-center">Расширенная гарантия на велосипеды MAVERICK –<br/>лучшее подтверждение качества</h2>
        </div>
    </section>
    <!-- -->

    <section class="warranty-block warranty-block-one">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="h1">Рама</div>
                    <p>Покупатель имеет право на бесплатный ремонт в случае обнаружения дефектов в местах сварки стальной рамы в течение 1 года, алюминиевой рамы – в течение 3 лет. Гарантия действительна при соблюдении всех условий эксплуатации, хранения и ухода за изделием.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="warranty-block warranty-block-two">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-6">
                    <div class="h1">Навесное<br/>оборудование</div>
                    <p>Покупатель имеет право на бесплатный ремонт в случае обнаружения дефектов в местах сварки передней жесткой вилки в течение 1 года, амортизационной вилки – в течение 3 месяцев.</p>
                    <p>Гарантийный срок эксплуатации заднего амортизатора – 3 месяца, прочих узлов и комплектующих – 6 месяцев. Все гарантийные обязательства несет изготовитель деталей.</p>
                    <p>Гарантия действительна при соблюдении всех условий эксплуатации, хранения и ухода за изделием.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="warranty-block warranty-block-three">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="h1">Заводская сборка</div>
                    <p>Настройка некоторых узлов (тормоза, трансмиссия) выполняется на заводе-производителе. Итоговая сборка велосипеда выполняется в официальных точках продажи Maverick, что обязательно указывается в паспорте изделия. Без такой пометки сервисного центра гарантия на велосипед не распространяется.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="branch">
        <div class="container">
            <a class="btn-toggle btn-switch svg-responsive" data-target=".panel-switch-inner">
                <img src="img/btn-toggle.svg" alt="">
            </a>

            <div class="panel-switch-inner">
                <div class="map-filter panel-switch">
                    <div class="map-filter-content">
                        <span class="map-filter-hide panel-switch-hide"></span>
                        <div class="map-filter-search">
                            <form class="form">
                                <input type="text" class="form-search" placeholder="Искать по названию">
                            </form>
                        </div>
                        <div class="map-filter-result">
                            <div class="result-none hide">Мы не смогли найти<br/>такой регион</div>
                            <div class="result-list">
                                <ul>
                                    <li>Набережные Челны</li>
                                    <li>Нижневартовск</li>
                                    <li>Нижний Тагил</li>
                                    <li>Новокузнецк</li>
                                    <li>Новокуйбышевск</li>
                                    <li>Новочеркасск</li>
                                    <li>Ноябрьск</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="map-content">
                <div class="h1">Сертифицированные<br/>сервис-центры</div>
                <div class="map-list hidden-xs">
                    <select class="form-control select-style" name="region">
                        <option>По всей России</option>
                        <option>Москва</option>
                        <option>Санкт-Петербург</option>
                        <option>Тамбов</option>
                        <option>Волгоград</option>
                        <option>Орел</option>
                    </select>
                </div>
            </div>
        </div>
        <div id="wmap"></div>

    </section>

    <!-- Footer Banner -->
    <?php include('inc/promo.php') ?>
    <!-- -->

    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->

    <!-- Modal -->
        <div class="hide">
            <div class="modal friends-block" id="friends">
                <div class="modal-border">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="#" class="modal-image">
                                <img src="images/product01.jpg" class="img-responsive" alt="">
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <div class="modal-text">
                                <div class="modal-title">«Привет! Я выбираю себе велик. Как тебе такой вариант?»</div>
                                <h4><a href="#">Maverick X29</a></h4>
                                <ul class="product-info">
                                    <li>Рама: Сталь Hi-Ten</li>
                                    <li>Количество скоростей: 6</li>
                                    <li>Размер колес: 26"</li>
                                    <li>Тормоза: Ободные (V-Brake)</li>
                                    <li>Вес: 14,7 кг</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="btn-friends clearfix">
                    <li><a href="#" class="btn btn-sm btn-fb"><i class="fa fa-facebook" aria-hidden="true"></i> Отправить</a></li>
                    <li><a href="#" class="btn btn-sm btn-vk"><i class="fa fa-vk" aria-hidden="true"></i> Отправить</a></li>
                    <li><a href="#" class="btn btn-sm btn-tw"><i class="fa fa-twitter" aria-hidden="true"></i> Отправить</a></li>
                    <li><a href="#" class="btn btn-sm btn-ok"><i class="fa fa-odnoklassniki" aria-hidden="true"></i> Отправить</a></li>
                    <li><a href="#" class="btn btn-sm btn-gp"><i class="fa fa-google-plus" aria-hidden="true"></i> Отправить</a></li>
                </ul>
            </div>
        </div>
    <!-- -->

    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    <script src="js/service.map.js"></script>

</body>
</html>


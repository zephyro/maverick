<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

            <ul class="breadcrumbs">
                <li><a href="#">Главная</a></li>
            </ul>

            <h1>Справочный центр</h1>

            <div class="tabs">
                <ul class="tabs-nav help-nav">
                    <li class="active"><a href="#" data-target=".tab1">Устройство велосипеда</a></li>
                    <li><a href="#" data-target=".tab2">Подбор рамы по росту</a></li>
                    <li><a href="#" data-target=".tab3">Техническое обслуживание</a></li>
                </ul>

                <div class="tabs-item tab1 active">
                    <div class="construction clearfix">

                        <div class="construction-content">
                            <ul class="construction-image">
                                <li class="detail01 active">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail02">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail03">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail04">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail05">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail06">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail07">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail08">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail09">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail10">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail11">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail12">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail13">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail14">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail15">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail16">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail17">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail18">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                                <li class="detail19">
                                    <img src="images/bike/bike01.png" alt="" class="img-responsive">
                                </li>
                            </ul>

                            <ul class="construction-description">
                                <li class="detail01 active">
                                    <h2>Вилка</h2>
                                    <p>Вилка – это важная деталь велосипеда, которая отвечает за качества хода и амортизационные характеристики. Жесткие вилки из титана, стали, алюминия и других материалов сегодня используются в основном на специальных моделях велосипедов, которые рассчитаны на скоростную езду по идеально ровной поверхности. Чаще всего встречаются амортизационные вилки с подвижным соединением деталей различной конструкции. На моделях с амортизационной вилкой хорошо ощущается контакт колес с поверхностью, сохраняется контроль над велосипедом при езде по кочкам и ухабам.</p>
                                    <ul class="construction-gallery">
                                        <li>
                                            <span>
                                                <img src="images/bike/plug01.jpg" alt="" class="img-responsive">
                                            </span>
                                        </li>
                                        <li>
                                            <span>
                                                <img src="images/bike/plug02.jpg" alt="" class="img-responsive">
                                            </span>
                                        </li>
                                        <li>
                                            <span>
                                                <img src="images/bike/plug03.jpg" alt="" class="img-responsive">
                                            </span>
                                        </li>
                                        <li>
                                            <span>
                                                <img src="images/bike/plug04.jpg" alt="" class="img-responsive">
                                            </span>
                                        </li>
                                    </ul>
                                </li>
                                <li class="detail02">
                                    <h2>Рама</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail03">
                                    <h2>Тормоза</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail04">
                                    <h2>Рулевая колонка</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail05">
                                    <h2>Вынос руля</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail06">
                                    <h2>Руль</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail07">
                                    <h2>Задний переключатель</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail08">
                                    <h2>Передний переключатель</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail09">
                                    <h2>Шифтеры</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail10">
                                    <h2>Система шатунов</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail11">
                                    <h2>Каретка</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail12">
                                    <h2>Трещотка / Кассета</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail13">
                                    <h2>Цепь</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail14">
                                    <h2>Втулка колеса задняя</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail15">
                                    <h2>Втулка колеса передняя</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail16">
                                    <h2>Обода</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail17">
                                    <h2>Покрышки</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail18">
                                    <h2>Седло</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                                <li class="detail19">
                                    <h2>Подседельный штырь</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus sem, commodo et sodales quis, rutrum id ex. Vestibulum condimentum nulla in nisi efficitur, quis tempus purus maximus. Morbi nec sodales magna. Sed lobortis nunc felis. In eget aliquam leo, eget dapibus odio. Quisque eleifend lectus at lectus iaculis, eu malesuada lorem pretium. Phasellus dui magna, dignissim ut metus ac, consequat suscipit felis.</p>
                                </li>
                            </ul>
                        </div>

                        <div class="construction-filter">
                            <ul>
                                <li class="active"><a href="#" data-target=".detail01">Вилка</a></li>
                                <li><a href="#" data-target=".detail02">Рама</a></li>
                                <li><a href="#" data-target=".detail03">Тормоза</a></li>
                                <li><a href="#" data-target=".detail04">Рулевая колонка</a></li>
                                <li><a href="#" data-target=".detail05">Вынос руля</a></li>
                                <li><a href="#" data-target=".detail06">Руль</a></li>
                                <li><a href="#" data-target=".detail07">Задний переключатель</a></li>
                                <li><a href="#" data-target=".detail08">Передний переключатель</a></li>
                                <li><a href="#" data-target=".detail09">Шифтеры</a></li>
                                <li><a href="#" data-target=".detail10">Система шатунов</a></li>
                                <li><a href="#" data-target=".detail11">Каретка</a></li>
                                <li><a href="#" data-target=".detail12">Трещотка / Кассета</a></li>
                                <li><a href="#" data-target=".detail13">Цепь</a></li>
                                <li><a href="#" data-target=".detail14">Втулка колеса задняя</a></li>
                                <li><a href="#" data-target=".detail15">Втулка колеса передняя</a></li>
                                <li><a href="#" data-target=".detail16">Обода</a></li>
                                <li><a href="#" data-target=".detail17">Покрышки</a></li>
                                <li><a href="#" data-target=".detail18">Седло</a></li>
                                <li><a href="#" data-target=".detail19">Подседельный штырь</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="tabs-item tab2">
                    <div class="row help-height">
                        <div class="col-md-6">
                            <div class="help-height-image svg-responsive">
                                <img src="img/bike-height.svg">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="help-height-text">
                                <h2>Как подобрать ростовку велосипеда</h2>
                                <p>Встаньте на ровную поверхность, расположив велосипед между ног, и оцените, сколько сантиметров осталось между Вашей промежностьюи верхней трубой рамы.</p>
                                <p>Нормой считается, если это расстояние составляет около 10-12 сантиметров (ширина ладони).</p>
                            </div>
                        </div>
                    </div>

                    <div class="container-responsive">
                        <div class="help-content">
                            <h3>Взрослые и подростки</h3>
                            <table>
                                <tr>
                                    <th>Рост велосипедиста (см)</th>
                                    <td>до 130</td>
                                    <td>135–155</td>
                                    <td>150–165</td>
                                    <td>160–175</td>
                                    <td>170–185</td>
                                    <td>180–190</td>
                                    <td>180–195</td>
                                </tr>
                                <tr>
                                    <th>Размер рамы (дюймы)</th>
                                    <td>11,5”</td>
                                    <td>14”</td>
                                    <td>16”</td>
                                    <td>17”</td>
                                    <td>19”</td>
                                    <td>20”</td>
                                    <td>21”</td>
                                </tr>
                            </table>

                            <h3>Дети и подростки</h3>
                            <table>
                                <tr>
                                    <th></th>
                                    <td>2–4 года</td>
                                    <td>3–5 лет</td>
                                    <td>4–6 лет</td>
                                    <td>5–7 лет</td>
                                    <td>6–9 лет</td>
                                    <td>9-13 лет</td>
                                </tr>
                                <tr>
                                    <th>Рост велосипедиста (см)</th>
                                    <td>85–100</td>
                                    <td>90–100</td>
                                    <td>100–125</td>
                                    <td>105–130</td>
                                    <td>115–140</td>
                                    <td>125–155</td>
                                </tr>
                                <tr>
                                    <th>Размер рамы (дюймы)</th>
                                    <td>12”</td>
                                    <td>14”</td>
                                    <td>16”</td>
                                    <td>18”</td>
                                    <td>20”</td>
                                    <td>24”</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="tabs-item tab3">
                    <h2>Техническое обслуживание</h2>
                    <p>Nam non libero eget mauris aliquam efficitur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum semper nec eros non mattis. Aliquam aliquet, turpis sit amet venenatis dapibus, purus ante congue felis, in blandit est justo nec urna. Mauris felis urna, lacinia sed tellus in, feugiat tincidunt eros. Ut laoreet et felis et elementum. In tincidunt odio quis tristique eleifend. Praesent ut lorem felis. Fusce turpis elit, ultricies sed rutrum sit amet, varius vel diam. Fusce posuere tortor metus, vitae mattis dui efficitur in. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla vitae viverra dolor, finibus fermentum lorem.</p>
                    <p>Phasellus sed tortor at nisi suscipit luctus. Morbi vel mattis sapien, ut mattis nibh. Cras id placerat ligula. Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla vitae euismod eros, non facilisis quam. Mauris ut leo volutpat purus venenatis elementum. Sed tristique dui eu leo molestie tempor. Praesent nisi diam, volutpat sed risus ac, lacinia convallis leo. Donec sagittis leo eu tempor porta.</p>
                </div>
            </div>

        </div>
    </section>

    <!-- Footer Banner -->
    <?php include('inc/promo.php') ?>
    <!-- -->

    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->



    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    </body>
</html>

<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->

    <div class="account-header">
        <div class="container">
            <div class="t-row">
                <div class="t-col">Иван Олегович Тинькофф</div>
            </div>
            <span class="account-toggle btn-switch" data-target=".panel-switch-inner">\
                <img src="img/account-toggle.svg" alt="">
            </span>
        </div>
    </div>

    <section class="account">
        <div class="container">
            <div class="clearfix">

                <div class="account-body">
                    <h1>Новости и акции</h1>

                    <article class="post">
                        <div class="post-image">
                            <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                        </div>
                        <span class="post-date">258 мая 2016</span>
                        <ul class="post-tags">
                            <li><a href="#">акция</a></li>
                            <li><a href="#">снижение цены</a></li>
                        </ul>
                        <h2><a href="#">Велошколы для пенсионеров в Санкт-Петербурге</a></h2>
                        <div class="post-preview">
                            Почти в каждом районе Северной столицы заработали специальные экологические кружки для пенсионеров – любой желающий может бесплатно получить уроки езды на велосипеде! Инструкторы объясняют слушателям правила...
                        </div>
                    </article>

                    <article class="post">
                        <div class="post-image">
                            <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                        </div>
                        <span class="post-date">258 мая 2016</span>
                        <ul class="post-tags">
                            <li><a href="#">акция</a></li>
                            <li><a href="#">снижение цены</a></li>
                        </ul>
                        <h2><a href="#">Велошколы для пенсионеров в Санкт-Петербурге</a></h2>
                        <div class="post-preview">
                            Почти в каждом районе Северной столицы заработали специальные экологические кружки для пенсионеров – любой желающий может бесплатно получить уроки езды на велосипеде! Инструкторы объясняют слушателям правила...
                        </div>
                    </article>

                    <article class="post">
                        <div class="post-image">
                            <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                        </div>
                        <span class="post-date">258 мая 2016</span>
                        <ul class="post-tags">
                            <li><a href="#">акция</a></li>
                            <li><a href="#">снижение цены</a></li>
                        </ul>
                        <h2><a href="#">Велошколы для пенсионеров в Санкт-Петербурге</a></h2>
                        <div class="post-preview">
                            Почти в каждом районе Северной столицы заработали специальные экологические кружки для пенсионеров – любой желающий может бесплатно получить уроки езды на велосипеде! Инструкторы объясняют слушателям правила...
                        </div>
                    </article>

                    <a href="#" class="btn-view">Показать еще</a>
                    <ul class="pagination">
                        <li class="begin"><a href="#">В начало</a></li>
                        <li class="prev"><a href="#">←</a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li class="next"><a href="#">→</a></li>
                    </ul>
                </div>

                <div class="account-side">
                    <div class="panel-switch-inner">
                        <div class="account-profile panel-switch">
                            <div class="ap-header">
                                <div class="ap-first-name">Иван Олегович</div>
                                <div class="ap-last-name">Тинькофф</div>
                                <div class="ap-company">ИП Тинькофф</div>
                            </div>

                            <div class="ap-body">
                                <ul class="ap-nav">
                                    <li class="item1"><a href="#">Получить доступный остаток</a></li>
                                    <li class="item2"><a href="#">Скачать каталог MAVERICK</a></li>
                                    <li class="item3"><a href="#">Поддержка клиента</a></li>
                                </ul>
                                <div class="ap-divider"></div>
                                <ul class="ap-nav">
                                    <li class="item4"><a href="#">Личные данные</a></li>
                                    <li class="item5"><a href="#">Выход</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="account-action">
                        <h3>Акции</h3>

                        <div class="account-action-slider">

                            <div class="account-action-item clearfix">
                                <div class="account-action-image">
                                    <img src="images/action-img.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="account-action-text">
                                    <h4><a href="#">Скидка на серию Х</a></h4>
                                    <p>Скидка на все  двухподвесы серии Х</p>
                                    <div class="account-action-date">20 мая 2016</div>
                                </div>
                            </div>

                            <div class="account-action-item clearfix">
                                <div class="account-action-image">
                                    <img src="images/action-img.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="account-action-text">
                                    <h4><a href="#">Изменение цен</a></h4>
                                    <p>Внимание! 05.06.206 изменились цены на модели в категориях “дорожные” и “подростковые”</p>
                                    <div class="account-action-date">20 мая 2016</div>
                                </div>
                            </div>

                            <div class="account-action-item clearfix">
                                <div class="account-action-image">
                                    <img src="images/action-img.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="account-action-text">
                                    <h4><a href="#">Новая модель GTR</a></h4>
                                    <p>Рады представить вам новую модель GTR</p>
                                    <div class="account-action-date">20 мая 2016</div>
                                </div>
                            </div>
                        </div>

                        <a href="#" class="btn-blue btn-blue-md">Смотреть все акции</a>
                    </div>

                </div>
            </div>
        </div>
    </section>


    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->

    <!-- Modal -->
    <div class="hide">
        <div class="modal modal-sm" id="question">
            <div class="modal-header">Поддержка клиента</div>
            <div class="modal-body">
                <h3>Поддержка клиента</h3>
                <form class="form">
                    <div class="form-group">
                        <input type="text" name="" class="form-control" placeholder="Ваше имя">
                    </div>
                    <div class="form-group">
                        <input type="text" name="" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <textarea name="message" class="form-control" placeholder="Текст сообщения" rows="4"></textarea>
                    </div>
                    <div class="pb20"></div>
                    <button type="submit" class="btn btn-send">Отправить</button>
                </form>
            </div>
        </div>
    </div>
    <!-- -->

    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    </body>
</html>

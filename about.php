<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.php') ?>
    </head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->

    <!--  -->
    <section class="main">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="#">Главная</a></li>
            </ul>
            <h1>О Компании</h1>

            <div class="content">Знаменитый бренд Маверик вышел на российский рынок велосипедов в 2013 году. Первоначально мы представили покупателям линейку приемлемых по цене горных байков для взрослых и детей. Предложенная продукция быстро завоевала доверие потребителей, и сейчас велосипеды  Maverick известны уже не только на своей родине, в России, но и далеко за ее пределами.</div>


            <article class="about tabs">

                <ul class="about-nav tabs-nav">
                    <li class="item1 active"><a href="#" data-target=".tab1">Миссия компании</a></li>
                    <li class="item2"><a href="#" data-target=".tab2">Сервис и отношение к клиентам</a></li>
                    <li class="item3"><a href="#" data-target=".tab3">Производство велосипедов Maveric</a></li>
                </ul>

                <div class="about-content">
                    <div class="tabs-item tab1 active">
                        <h2>Миссия компании</h2>
                        <p>Praesent risus mi, cursus quis augue tincidunt, consequat vehicula augue. Integer semper egestas sapien, vitae aliquet diam. Proin quis diam lorem. Sed libero nisl, convallis et aliquam at, ultrices a sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                    </div>
                    <div class="tabs-item tab2">
                        <h2>Сервис и отношение к клиентам</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et metus neque. Duis ornare diam quis nulla pulvinar, in porttitor mi sagittis. Morbi justo metus, sollicitudin et lacus eu, bibendum dapibus diam. In porttitor magna et sapien ultrices imperdiet. Aliquam nibh tortor, tempus et malesuada sed, tincidunt eget magna. Donec at diam libero. Nulla eget est rutrum, molestie orci non, semper nibh.</p>
                    </div>
                    <div class="tabs-item tab3">
                        <h2>Производство велосипедов Maverick</h2>
                        <p>Велосипеды Maverick производятся нами в России в г. Калининграде уже в течение 3 лет. За время работы компания значительно выросла. Сейчас она состоит из … сотрудников и производит 4 вида велосипедов: горные хардтейлы, горные двухподвесы, дорожные и подростковые. В ближайшее время ассортимент производимых велосипедов мы планируем значительно расширить.</p>
                        <p>Каждый этап сборки продукции тщательно контролируется у нас специальной службой. Это обеспечивает отличный результат (как показывает практика, наши велосипеды в 5 раз надежнее прочих современных аналогов, представленных сегодня на велосипедном рынке России).</p>
                        <div class="about-gallery">
                            <div class="about-slider">
                                <div class="about-slider-item">
                                    <img src="images/about-img_01.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="about-slider-item">
                                    <img src="images/about-img_01.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="about-slider-item">
                                    <img src="images/about-img_01.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="about-slider-item">
                                    <img src="images/about-img_01.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="about-slider-item">
                                    <img src="images/about-img_01.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="about-slider-item">
                                    <img src="images/about-img_01.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="about-slider-item">
                                    <img src="images/about-img_01.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="about-slider-item">
                                    <img src="images/about-img_01.jpg" alt="" class="img-responsive">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

        </div>
    </section>
    <!-- -->

    <section class="staff">
        <div class="container">
            <div class="staff-slider">
                <div class="staff-item clearfix">
                    <div class="staff-photo">
                        <div class="staff-photo-inner">
                            <img src="images/staff01.jpg" alt="" class="img-responsive">
                        </div>
                    </div>
                    <div class="staff-text">
                        <div class="staff-name">Роман <span>Смоляков</span></div>
                        <div class="staff-position">Директор</div>
                        <p>Родился в 1978 году Перми. Окончил Пермский государственный университет.</p>
                        <p>Имеет MBA от Moscow Business School. Управленец. Колоссальный опытв работе с велосипедами.</p>
                        <p>В «Maverick» с 2013 года. Женат. Двое детей. Хобби: горные лыжи, чтение прессы.</p>
                    </div>
                </div>

                <div class="staff-item clearfix">
                    <div class="staff-photo">
                        <div class="staff-photo-inner">
                            <img src="images/staff01.jpg" alt="" class="img-responsive">
                        </div>
                    </div>
                    <div class="staff-text">
                        <div class="staff-name">Роман <span>Смоляков</span></div>
                        <div class="staff-position">Директор</div>
                        <p>Родился в 1978 году Перми. Окончил Пермский государственный университет.</p>
                        <p>Имеет MBA от Moscow Business School. Управленец. Колоссальный опытв работе с велосипедами.</p>
                        <p>В «Maverick» с 2013 года. Женат. Двое детей. Хобби: горные лыжи, чтение прессы.</p>
                    </div>
                </div>

                <div class="staff-item clearfix">
                    <div class="staff-photo">
                        <div class="staff-photo-inner">
                            <img src="images/staff01.jpg" alt="" class="img-responsive">
                        </div>
                    </div>
                    <div class="staff-text">
                        <div class="staff-name">Роман <span>Смоляков</span></div>
                        <div class="staff-position">Директор</div>
                        <p>Родился в 1978 году Перми. Окончил Пермский государственный университет.</p>
                        <p>Имеет MBA от Moscow Business School. Управленец. Колоссальный опытв работе с велосипедами.</p>
                        <p>В «Maverick» с 2013 года. Женат. Двое детей. Хобби: горные лыжи, чтение прессы.</p>
                    </div>
                </div>

                <div class="staff-item clearfix">
                    <div class="staff-photo">
                        <div class="staff-photo-inner">
                            <img src="images/staff01.jpg" alt="" class="img-responsive">
                        </div>
                    </div>
                    <div class="staff-text">
                        <div class="staff-name">Роман <span>Смоляков</span></div>
                        <div class="staff-position">Директор</div>
                        <p>Родился в 1978 году Перми. Окончил Пермский государственный университет.</p>
                        <p>Имеет MBA от Moscow Business School. Управленец. Колоссальный опытв работе с велосипедами.</p>
                        <p>В «Maverick» с 2013 года. Женат. Двое детей. Хобби: горные лыжи, чтение прессы.</p>
                    </div>
                </div>
            </div>

            <span class="staff-nav next"></span>
        </div>
    </section>

    <section class="evolution">
        <div class="container">
            <h2>Развитие компании «MAVERICK»</h2>
            <div class="evolution-responsive">
                <ul class="evolution-items clearfix">
                    <li class="item1">
                        <div class="evolution-year">2013</div>
                        <div class="evolution-title">старт</div>
                        <div class="evolution-text">Компания выпускает линейку горных велосипедов MAVERICKдля взрослых и детей.</div>
                    </li>
                    <li class="item2">
                        <div class="evolution-year">2014</div>
                        <div class="evolution-title">повторяем успех</div>
                        <div class="evolution-text">После успешных продаж горных велосипедов MAVERICK по всей России, компания запускает в производство серию городских байков.</div>
                    </li>
                    <li class="item3">
                        <div class="evolution-year">2015</div>
                        <div class="evolution-title">набираем обороты</div>
                        <div class="evolution-text">Всю прибыль до копейки, компания вкладывает в качество велосипедов MAVERICK, снижая при этом их стоимость</div>
                    </li>
                    <li class="item4">
                        <div class="evolution-year">2016</div>
                        <div class="evolution-title">продолжаем крутить педали</div>
                        <div class="evolution-text">Мы уже нарисовали прототипы абсолютно новых велосипедов класса Супершоссе на стальной раме</div>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <!-- Footer Banner -->
    <?php include('inc/promo.php') ?>
    <!-- -->

    <!-- Footer -->
    <?php include('inc/footer.php') ?>

    <!-- -->
    <!-- Modal -->
        <div class="hide">
            <div class="modal friends-block" id="friends">
                <div class="modal-border">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="#" class="modal-image">
                                <img src="images/product01.jpg" class="img-responsive" alt="">
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <div class="modal-text">
                                <div class="modal-title">«Привет! Я выбираю себе велик. Как тебе такой вариант?»</div>
                                <h4><a href="#">Maverick X29</a></h4>
                                <ul class="product-info">
                                    <li>Рама: Сталь Hi-Ten</li>
                                    <li>Количество скоростей: 6</li>
                                    <li>Размер колес: 26"</li>
                                    <li>Тормоза: Ободные (V-Brake)</li>
                                    <li>Вес: 14,7 кг</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="btn-friends clearfix">
                    <li><a href="#" class="btn btn-sm btn-fb"><i class="fa fa-facebook" aria-hidden="true"></i> Отправить</a></li>
                    <li><a href="#" class="btn btn-sm btn-vk"><i class="fa fa-vk" aria-hidden="true"></i> Отправить</a></li>
                    <li><a href="#" class="btn btn-sm btn-tw"><i class="fa fa-twitter" aria-hidden="true"></i> Отправить</a></li>
                    <li><a href="#" class="btn btn-sm btn-ok"><i class="fa fa-odnoklassniki" aria-hidden="true"></i> Отправить</a></li>
                    <li><a href="#" class="btn btn-sm btn-gp"><i class="fa fa-google-plus" aria-hidden="true"></i> Отправить</a></li>
                </ul>
            </div>
        </div>
    <!-- -->

    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->


    </body>
</html>


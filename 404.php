<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.php') ?>
    </head>

    <body>

    <section class="page404">
        <div class="container">
            <a href="#" class="logo svg-responsive">
                <img src="img/logo.svg" alt="">
            </a>

            <div class="img404">
                <img src="img/404_bg.svg" alt="" class="svg-responsive">
            </div>
            <h1>Ошибка 404</h1>
            <div class="text404">
                Запрашиваемая страница не найдена.<br/>
                Вы можете вернуться на <a href="/">главную</a> страницу или <a href="#">найти</a> себе велик Maverick.
            </div>
            <div class="text-center">
                <div class="word404">#Error</div>
            </div>
        </div>
    </section>


    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    </body>
</html>

<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->

    <header class="cat-header">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="#">Главная</a></li>
                <li>Каталог</li>
            </ul>
            <div class="heading">
                <span class="heading-primary">Дорожные</span>
                <span class="heading-second">#onroad</span>
            </div>
            <div class="cat-detail">42 модели от 23 799руб.</div>
        </div>
    </header>

    <section class="main">
        <div class="container">
            <div class="rows clearfix">

                <!-- Фильтр -->
                <?php include('inc/filter.php') ?>
                <!--  -->

                <!-- Каталог -->
                <div class="cat">
                    <div class="top-cat">
                        <div class="row">
                            <div class="col-sm-5">
                                <form class="form">
                                    <input type="text" class="form-search" placeholder="Искать по названию">
                                </form>
                            </div>
                            <div class="col-sm-7 hidden-xs clearfix">
                                <ul class="cat-view clearfix">
                                    <li class="active" data-target="1">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
                                            <defs>
                                                <style>

                                                </style>
                                            </defs>
                                            <title>Ресурс 2 1</title>
                                            <g id="layer1" data-name="layer1">
                                                <g id="Layer_1" data-name="Layer 1">
                                                    <rect class="cls-1" width="4" height="4"/>
                                                    <rect class="cls-1" x="6" width="4" height="4"/>
                                                    <rect class="cls-1" x="12" width="4" height="4"/>
                                                    <rect class="cls-1" y="6" width="4" height="4"/>
                                                    <rect class="cls-1" x="6" y="6" width="4" height="4"/>
                                                    <rect class="cls-1" x="12" y="6" width="4" height="4"/>
                                                    <rect class="cls-1" y="12" width="4" height="4"/>
                                                    <rect class="cls-1" x="6" y="12" width="4" height="4"/>
                                                    <rect class="cls-1" x="12" y="12" width="4" height="4"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </li>
                                    <li data-target="2">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
                                            <defs>
                                                <style>

                                                </style>
                                            </defs>
                                            <title>Ресурс 2</title>
                                            <g id="layer2" data-name="layer2">
                                                <g id="Layer_2" data-name="Layer 2">
                                                    <rect class="cls-1" width="16" height="4"/>
                                                    <rect class="cls-1" y="6" width="16" height="4"/>
                                                    <rect class="cls-1" y="12" width="16" height="4"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </li>
                                </ul>
                                <div class="sort-by">
                                    <div class="wrapper-dropdown" id="dd">
                                        <span class="dropdown-name">по убыванию цены</span>
                                        <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
                                        <div class="list-dropdown">
                                            <span>сортировать по</span>
                                            <ul>
                                                <li><a href="#">по убыванию цены</a></li>
                                                <li><a href="#">по возрастанию цены</a></li>
                                                <li><a href="#">по алфавиту</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="sort-title">Сортировать</div>
                            </div>
                        </div>
                    </div>

                    <div class="showcase clearfix">

                        <!-- Элемент каталога -->
                        <div class="showcase-item">
                            <div class="showcase-col col-one">
                                <a href="#" class="showcase-image">
                                    <img src="images/product01.jpg" alt="" class="img-responsive">
                                </a>
                                <span class="showcase-year">2016</span>
                            </div>
                            <div class="showcase-col col-two">
                                <h4><a href="#">Maverick K41</a></h4>
                                <ul class="showcase-params">
                                    <li>Сезон: 2016</li>
                                    <li>Ростовка: 7,5"/19"</li>
                                    <li>Количество скоростей: 27</li>
                                    <li>Материал рамы: Алюминий</li>
                                </ul>
                            </div>
                            <div class="showcase-col col-three">
                                <div class="showcase-price">
                                    <div class="price-value">23 700 руб</div>
                                    <div class="price-text"><span>рекомендуемая цена <i class="btn-tooltip" rel="tooltip" title="цена в вашем городе может быть выше">?</i></span></div>
                                </div>
                                <a href="#" class="btn-blue">Подробнее</a>
                            </div>
                        </div><!-- -->

                        <!-- Элемент каталога -->
                        <div class="showcase-item">
                            <div class="showcase-col col-one">
                                <a href="#" class="showcase-image">
                                    <img src="images/product01.jpg" alt="" class="img-responsive">
                                </a>
                                <span class="showcase-year">2016</span>
                            </div>
                            <div class="showcase-col col-two">
                                <h4><a href="#">Maverick K41</a></h4>
                                <ul class="showcase-params">
                                    <li>Сезон: 2016</li>
                                    <li>Ростовка: 7,5"/19"</li>
                                    <li>Количество скоростей: 27</li>
                                    <li>Материал рамы: Алюминий</li>
                                </ul>
                            </div>
                            <div class="showcase-col col-three">
                                <div class="showcase-price">
                                    <div class="price-value">23 700 руб</div>
                                    <div class="price-text"><span>рекомендуемая цена <i class="btn-tooltip" rel="tooltip" title="цена в вашем городе может быть выше">?</i></span></div>
                                </div>
                                <a href="#" class="btn-blue">Подробнее</a>
                            </div>
                        </div><!-- -->

                        <!-- Элемент каталога -->
                        <div class="showcase-item">
                            <div class="showcase-col col-one">
                                <a href="#" class="showcase-image">
                                    <img src="images/product01.jpg" alt="" class="img-responsive">
                                </a>
                                <span class="showcase-year">2016</span>
                            </div>
                            <div class="showcase-col col-two">
                                <h4><a href="#">Maverick K41</a></h4>
                                <ul class="showcase-params">
                                    <li>Сезон: 2016</li>
                                    <li>Ростовка: 7,5"/19"</li>
                                    <li>Количество скоростей: 27</li>
                                    <li>Материал рамы: Алюминий</li>
                                </ul>
                            </div>
                            <div class="showcase-col col-three">
                                <div class="showcase-price">
                                    <div class="price-value">23 700 руб</div>
                                    <div class="price-text"><span>рекомендуемая цена <i class="btn-tooltip" rel="tooltip" title="цена в вашем городе может быть выше">?</i></span></div>
                                </div>
                                <a href="#" class="btn-blue">Подробнее</a>
                            </div>
                        </div><!-- -->

                        <!-- Элемент каталога -->
                        <div class="showcase-item">
                            <div class="showcase-col col-one">
                                <a href="#" class="showcase-image">
                                    <img src="images/product01.jpg" alt="" class="img-responsive">
                                </a>
                                <span class="showcase-year">2016</span>
                            </div>
                            <div class="showcase-col col-two">
                                <h4><a href="#">Maverick K41</a></h4>
                                <ul class="showcase-params">
                                    <li>Сезон: 2016</li>
                                    <li>Ростовка: 7,5"/19"</li>
                                    <li>Количество скоростей: 27</li>
                                    <li>Материал рамы: Алюминий</li>
                                </ul>
                            </div>
                            <div class="showcase-col col-three">
                                <div class="showcase-price">
                                    <div class="price-value">23 700 руб</div>
                                    <div class="price-text"><span>рекомендуемая цена <i class="btn-tooltip" rel="tooltip" title="цена в вашем городе может быть выше">?</i></span></div>
                                </div>
                                <a href="#" class="btn-blue">Подробнее</a>
                            </div>
                        </div><!-- -->

                        <!-- Элемент каталога -->
                        <div class="showcase-item">
                            <div class="showcase-col col-one">
                                <a href="#" class="showcase-image">
                                    <img src="images/product01.jpg" alt="" class="img-responsive">
                                </a>
                                <span class="showcase-year">2016</span>
                            </div>
                            <div class="showcase-col col-two">
                                <h4><a href="#">Maverick K41</a></h4>
                                <ul class="showcase-params">
                                    <li>Сезон: 2016</li>
                                    <li>Ростовка: 7,5"/19"</li>
                                    <li>Количество скоростей: 27</li>
                                    <li>Материал рамы: Алюминий</li>
                                </ul>
                            </div>
                            <div class="showcase-col col-three">
                                <div class="showcase-price">
                                    <div class="price-value">23 700 руб</div>
                                    <div class="price-text"><span>рекомендуемая цена <i class="btn-tooltip" rel="tooltip" title="цена в вашем городе может быть выше">?</i></span></div>
                                </div>
                                <a href="#" class="btn-blue">Подробнее</a>
                            </div>
                        </div><!-- -->

                        <!-- Элемент каталога -->
                        <div class="showcase-item">
                            <div class="showcase-col col-one">
                                <a href="#" class="showcase-image">
                                    <img src="images/product01.jpg" alt="" class="img-responsive">
                                </a>
                                <span class="showcase-year">2016</span>
                            </div>
                            <div class="showcase-col col-two">
                                <h4><a href="#">Maverick K41</a></h4>
                                <ul class="showcase-params">
                                    <li>Сезон: 2016</li>
                                    <li>Ростовка: 7,5"/19"</li>
                                    <li>Количество скоростей: 27</li>
                                    <li>Материал рамы: Алюминий</li>
                                </ul>
                            </div>
                            <div class="showcase-col col-three">
                                <div class="showcase-price">
                                    <div class="price-value">23 700 руб</div>
                                    <div class="price-text"><span>рекомендуемая цена <i class="btn-tooltip" rel="tooltip" title="цена в вашем городе может быть выше">?</i></span></div>
                                </div>
                                <a href="#" class="btn-blue">Подробнее</a>
                            </div>
                        </div><!-- -->

                    </div>

                    <a href="#" class="btn-view">Показать еще</a>

                    <ul class="pagination">
                        <li class="begin"><a href="#">В начало</a></li>
                        <li class="prev"><a href="#">&larr;</a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li class="next"><a href="#">&#8594;</a></li>
                    </ul>

                    <a href="#" class="bye-link">Велосипеды Maverick дорожные купить в Екатеринбурге</a>
                </div><!--  -->
            </div>

        </div>
    </section>


    <!-- Footer Banner -->
    <?php include('inc/promo.php') ?>
    <!-- -->

    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->


    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    </body>
</html>

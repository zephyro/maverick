
ymaps.ready(init);

function init () {

    var myMap = new ymaps.Map("wmap", {
            center: [61.2541,73.3962],
            zoom: 4,
            controls: ['smallMapDefaultSet']
        }),

        myGeoObject = new ymaps.GeoObject({
            // Описание геометрии.
            geometry: {
                type: "Point"
            },
            // Свойства.
            properties: {
                // Контент метки.
                iconContent: '',
                hintContent: ''
            }
        }, {
            preset: 'islands#blackStretchyIcon',
            draggable: true
        });

    myMap.behaviors.disable('scrollZoom');


    myMap.geoObjects
        .add(new ymaps.Placemark([55.7540,37.6204], {
            balloonContent: '<strong>Москва</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([59.9391,30.3159], {
            balloonContent: '<strong>Санкт-Петербург</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([43.1164,131.8825], {
            balloonContent: '<strong>Владивосток, RU</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([56.3269,44.0060], {
            balloonContent: '<strong>Нижний Новгород</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([53.1955,50.1018], {
            balloonContent: '<strong>Самара</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([45.0402,38.9760], {
            balloonContent: '<strong>Краснодар</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([48.4726,135.0577], {
            balloonContent: '<strong>Хабаровск, RU</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([52.9672,36.0696], {
            balloonContent: '<strong>Орёл, RU</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([57.1530,65.5343], {
            balloonContent: '<strong>Тюмень, RU</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([56.8386,60.6055], {
            balloonContent: '<strong>Екатеринбург, RU</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([51.6615,39.2003], {
            balloonContent: '<strong>Воронеж, RU</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([57.1530,65.5343], {
            balloonContent: '<strong>Тюмень</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([47.2225,39.7187], {
            balloonContent: '<strong>Ростов-на-Дону, RU</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([55.1603,61.4009], {
            balloonContent: '<strong>Челябинск, RU</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([44.9521,34.1024], {
            balloonContent: '<strong>Симферополь</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([46.3479,48.0336], {
            balloonContent: '<strong>Астрахань, RU</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([61.2541,73.3962], {
            balloonContent: '<strong>Сургут</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([51.5331,46.0342], {
            balloonContent: '<strong>Саратов, RU</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }))
        .add(new ymaps.Placemark([56.4847,84.9482], {
            balloonContent: '<strong>Томск, RU</strong>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [30, 37],
            iconImageOffset: [-15, -33]
        }));
}
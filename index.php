<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->

    <section class="slider">
        <div class="slider-item item1">
            <div class="container">
                <div class="heading">
                    <span class="heading-primary">Дорожные<br/>велосипеды</span>
                    <span class="heading-second">#onRoad</span>
                </div>
                <div class="slider-text">34 модели в каталоге</div>
                <a href="#" class="btn">Подробнее</a>
            </div>
        </div>
        <div class="slider-item item1">
            <div class="container">
                <div class="heading">
                    <span class="heading-primary">Дорожные<br/>велосипеды</span>
                    <span class="heading-second">#onRoad</span>
                </div>
                <div class="slider-text">34 модели в каталоге</div>
                <a href="#" class="btn">Подробнее</a>
            </div>
        </div>
        <div class="slider-item item1">
            <div class="container">
                <div class="heading">
                    <span class="heading-primary">Дорожные<br/>велосипеды</span>
                    <span class="heading-second">#onRoad</span>
                </div>
                <div class="slider-text">34 модели в каталоге</div>
                <a href="#" class="btn">Подробнее</a>
            </div>
        </div>
    </section>

    <section class="index-box-one">
        <div class="container">
            <h1 class="text-center">Велосипеды Maverick</h1>
            <ul class="bike-type clearfix">
                <li>
                    <img src="img/index_image_01.jpg" alt="" class="img-responsive">
                    <a href="#">
                        <div class="bike-content">
                            <div class="bike-title">Горные хардтейлы</div>
                            <div class="bike-detail">42 модели от 32 400Р</div>
                        </div>
                    </a>
                </li>
                <li>
                    <img src="img/index_image_02.jpg" alt="" class="img-responsive">
                    <a href="#">
                        <div class="bike-content">
                            <div class="bike-title">Горные двухподвесы</div>
                            <div class="bike-detail">42 модели от 32 400Р</div>
                        </div>
                    </a>
                </li>
                <li>
                    <img src="img/index_image_03.jpg" alt="" class="img-responsive">
                    <a href="#">
                        <div class="bike-content">
                            <div class="bike-title">Дорожные</div>
                            <div class="bike-detail">42 модели от 32 400Р</div>
                        </div>
                    </a>
                </li>
                <li>
                    <img src="img/index_image_04.jpg" alt="" class="img-responsive">
                    <a href="#">
                        <div class="bike-content">
                            <div class="bike-title">Подростковые</div>
                            <div class="bike-detail">42 модели от 32 400Р</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </section>

    <!-- Скачать Каталог -->
    <?php include('inc/index-box-two.php') ?>
    <!-- -->


    <!-- Bike Block -->
    <?php include('inc/bike-block.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">
            <div class="content">
                <div class="tabs text-tabs">
                    <div class="tabs-item tab1 active">
                        <h2>Почему выбирают MAVERICK</h2>
                        <p>Продажа велосипедов в Москве — как недорогих брендов (<a href="#">Cronus</a>,  <a href="#">Haro</a> ,  Stels,  Merida), так и брендов премиум-класса (Cube,  Bulls,  Trek,  Giant и т.п.) осуществляется нами по ценам значительно более низким,чем у конкурентов. С нашей помощью вы действительно сможете приобрести велосипеды в Москве недорого. Изучите наши предложения, и вы заметите, что цены вовсе не кусаются! И это не шутка – посмотрите стоимость байков и аксессуаров в нашем каталоге и приятно удивитесь</p>
                    </div>
                    <div class="tabs-item tab2">
                        <h2>Integer semper egestas sapien</h2>
                        <p>Praesent risus mi, cursus quis augue tincidunt, consequat vehicula augue. Integer semper egestas sapien, vitae aliquet diam. Proin quis diam lorem. Sed libero nisl, convallis et aliquam at, ultrices a sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                    </div>
                    <div class="tabs-item tab3">
                        <h2>Praesent risus mi, cursus quis augue tincidunt</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et metus neque. Duis ornare diam quis nulla pulvinar, in porttitor mi sagittis. Morbi justo metus, sollicitudin et lacus eu, bibendum dapibus diam. In porttitor magna et sapien ultrices imperdiet. Aliquam nibh tortor, tempus et malesuada sed, tincidunt eget magna. Donec at diam libero. Nulla eget est rutrum, molestie orci non, semper nibh.</p>
                    </div>

                    <ul class="tabs-nav clearfix">
                        <li class="active"><a href="#" data-target=".tab1">Велосипеды Maverick</a></li>
                        <li><a href="#" data-target=".tab2">Велосипеды оптом</a></li>
                        <li><a href="#" data-target=".tab3">Розничные магазины</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer Banner -->
    <?php include('inc/promo.php') ?>
    <!-- -->

    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->


    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    </body>
</html>


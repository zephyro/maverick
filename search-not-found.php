<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">

            <ul class="breadcrumbs">
                <li><a href="#">Главная</a></li>
                <li>Каталог</li>
            </ul>

            <h1>Поиск «К4» в каталоге</h1>
            <div class="search-detail">Мы нашли 2 модели от 23 799руб.</div>

            <div class="rows clearfix">

                <!-- Фильтр -->
                <?php include('inc/filter.php') ?>
                <!--  -->

                <!-- Каталог -->
                <div class="cat">
                    <div class="top-cat">
                        <div class="row">
                            <div class="col-sm-5">
                                <form class="form">
                                    <input type="text" class="form-search" placeholder="Искать по названию">
                                </form>
                            </div>
                            <div class="col-sm-7 hidden-xs clearfix">
                                <ul class="cat-view clearfix">
                                    <li class="active" data-target="1"></li>
                                    <li data-target="2"></li>
                                </ul>
                                <div class="sort-by">
                                    <div class="wrapper-dropdown" id="dd">
                                        <span class="dropdown-name">по убыванию цены</span>
                                        <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
                                        <div class="list-dropdown">
                                            <span>сортировать по</span>
                                            <ul>
                                                <li><a href="#">по убыванию цены</a></li>
                                                <li><a href="#">по возрастанию цены</a></li>
                                                <li><a href="#">по алфавиту</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="sort-title">Сортировать</div>
                            </div>
                        </div>
                    </div>

                    <div class="not-found">
                        <div class="found-title">Ничего не нашлось</div>
                        <p>Попробуйте ввести<br/>другой запрос</p>
                    </div>
                </div><!--  -->

            </div>

        </div>
    </section>

    <!-- Footer Banner -->
    <?php include('inc/promo.php') ?>
    <!-- -->

    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->


    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    </body>
</html>

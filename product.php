<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

<body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->

    <!--  -->
    <section class="main">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="#">Главная</a></li>
                <li><a href="#">Каталог</a></li>
                <li>Maverick X42</li>
            </ul>
            <h1>Maverick X42</h1>
        </div>

        <div class="product-gallery tabs">
            <div class="container">
                <a href="#" class="product-image">
                    <img src="images/product01.jpg" alt="" class="img-responsive">
                </a>

                <ul class="product-color tabs-nav clearfix">
                    <li class="active">
                        <a href="#">
                            <img src="img/filter-red.png" alt="" class="img-responsive">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="img/filter-white-gray.png" alt="" class="img-responsive">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="img/filter-green.png" alt="" class="img-responsive">
                        </a>
                    </li>
                </ul>

                <ul class="product-nav">
                    <li>
                        <a href="#">
                            <div class="product-nav-image">
                                <img src="images/product01.jpg" alt="" class="img-responsive">
                            </div>
                            <span>Maverick S24-M</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="product-nav-image">
                                <img src="images/product01.jpg" alt="" class="img-responsive">
                            </div>
                            <span>Maverick S45</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="product-description">
            <div class="container">
                <div class="product-buy">
                    <div class="product-price">23 700 руб</div>
                    <div class="product-price-text"><span>Рекомендуемая розничная цена<i class="btn-quest"></i></span></div>
                    <a href="#" class="btn">Где купить?</a>
                    <ul class="product-warranty">
                        <li>Гарантия на раму — 3 года,</li>
                        <li>на навесное оборудование — 1 год.</li>
                    </ul>
                </div>
                <div class="product-detail">
                    <ul class="item1">
                        <li><span>Ростовка</span></li>
                        <li>14"</li>
                    </ul>
                    <ul class="item2">
                        <li><span>Количество скоростей</span></li>
                        <li>6</li>
                    </ul>
                    <ul class="item3">
                        <li><span>Материал рамы</span></li>
                        <li>Сталь</li>
                    </ul>
                    <ul class="item4">
                        <li><span>Вилка</span></li>
                        <li>Сталь</li>
                    </ul>
                    <ul class="item5">
                        <li><span>Каретка</span></li>
                        <li>VP картридж</li>
                    </ul>
                    <ul class="item6">
                        <li><span>Тормоза</span></li>
                        <li>V-BRAKE, сталь</li>
                    </ul>
                    <ul class="item7">
                        <li><span>Система шатунов</span></li>
                        <li>Сталь, 40T</li>
                    </ul>

                    <div class="product-detail-hide">
                        <ul class="item8">
                            <li><span>Передний переключатель</span></li>
                            <li>-</li>
                        </ul>
                        <ul class="item9">
                            <li><span>Задний переключатель</span></li>
                            <li>SHIMANO, RD-TY21</li>
                        </ul>
                        <ul class="item10">
                            <li><span>Кассета/трещетка</span></li>
                            <li>SHIMANO, MF-TZ20</li>
                        </ul>
                        <ul class="item11">
                            <li><span>Крылья</span></li>
                            <li>Пластик</li>
                        </ul>
                        <ul class="item12">
                            <li><span>Левый шифтер</span></li>
                            <li>-</li>
                        </ul>
                        <ul class="item13">
                            <li><span>Правый шифтер</span></li>
                            <li>SHIMANO, SL-RS35</li>
                        </ul>
                        <ul class="item14">
                            <li><span>Цепь</span></li>
                            <li>KMC, Z33</li>
                        </ul>
                        <ul class="item15">
                            <li><span>Втулки</span></li>
                            <li>SF, сталь</li>
                        </ul>
                        <ul class="item16">
                            <li><span>Обод</span></li>
                            <li>DP-19A, двухстеночный</li>
                        </ul>
                        <ul class="item17">
                            <li><span>Покрышка</span></li>
                            <li>WANDA,P182, 24X1,95</li>
                        </ul>
                        <ul class="item18">
                            <li><span>Рулевая колонка</span></li>
                            <li>NECO, H800K</li>
                        </ul>
                        <ul class="item19">
                            <li><span>Вынос руля</span></li>
                            <li>Сталь</li>
                        </ul>
                        <ul class="item20">
                            <li><span>Руль</span></li>
                            <li>Сталь</li>
                        </ul>
                        <ul class="item21">
                            <li><span>Педали</span></li>
                            <li>VP, пластик</li>
                        </ul>
                        <ul class="item22">
                            <li><span>Сезон</span></li>
                            <li>2016</li>
                        </ul>

                    </div>
                    <div class="btn-group clearfix">
                        <a href="#" class="btn-blue btn-detail">Смотреть все характеристики</a>
                        <a href="#" class="btn-blue">Подобрать размер рамы</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="product-introtext">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7 col-lg-7">
                        <div class="product-text">
                            <p>Maverick X42 — лучшая модель для экстрима и активного отдыха. Эта горная модель обладает комплектацией, которая фактически близка к спортивным моделям, поэтому у нее повышенная прочность и износостойкость, а также более удачная эргономика. Рама Matts Speed разработана с учетом всех требований, чтобы подарить вам удобную и плавную езду.</p>
                            <a href="#" class="product-text-more">Узнать больше о Maverick X42</a>
                        </div>
                    </div>
                    <div class="col-sm-5 col-lg-5">
                        <div class="friends">
                            <div class="text-center">
                                <a href="#friends" class="btn btn-modal">Узнать мнение друзей</a>
                            </div>
                            <div class="friends-text"><span>#beWithFriends</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Похожие модели -->
        <div class="product-like">
            <div class="container">
                <h2>Другие модели этой серии</h2>
                <div class="product-row slider-like clearfix">

                    <div class="product-col">
                        <a href="#" class="product-image">
                            <img src="images/product01.jpg" alt="" class="img-responsive">
                        </a>
                        <span class="product-year">2016</span>
                        <h4><a href="#">Maverick K41</a></h4>
                        <div class="product-price">
                            <div class="price-value">23 700 руб</div>
                            <div class="price-text"><span>рекомендуемая цена <i class="btn-tooltip" rel="tooltip" title="цена в вашем городе может быть выше">?</i></span></div>
                        </div>

                    </div>
                    <div class="product-col">
                        <a href="#" class="product-image">
                            <img src="images/product01.jpg" alt="" class="img-responsive">
                        </a>
                        <span class="product-year">2016</span>
                        <h4><a href="#">Maverick K41</a></h4>
                        <div class="product-price">
                            <div class="price-value">23 700 руб</div>
                            <div class="price-text"><span>рекомендуемая цена <i class="btn-tooltip" rel="tooltip" title="цена в вашем городе может быть выше">?</i></span></div>
                        </div>

                    </div>
                    <div class="product-col">
                        <a href="#" class="product-image">
                            <img src="images/product01.jpg" alt="" class="img-responsive">
                        </a>
                        <span class="product-year">2016</span>
                        <h4><a href="#">Maverick K41</a></h4>
                        <div class="product-price">
                            <div class="price-value">23 700 руб</div>
                            <div class="price-text"><span>рекомендуемая цена <i class="btn-tooltip" rel="tooltip" title="цена в вашем городе может быть выше">?</i></span></div>
                        </div>

                    </div>
                    <div class="product-col">
                        <a href="#" class="product-image">
                            <img src="images/product01.jpg" alt="" class="img-responsive">
                        </a>
                        <span class="product-year">2016</span>
                        <h4><a href="#">Maverick K41</a></h4>
                        <div class="product-price">
                            <div class="price-value">23 700 руб</div>
                            <div class="price-text"><span>рекомендуемая цена <i class="btn-tooltip" rel="tooltip" title="цена в вашем городе может быть выше">?</i></span></div>
                        </div>

                    </div>
                </div>
            </div><!-- -->
        </div>

    </section>
    <!-- -->

    <!-- Footer Banner -->
    <?php include('inc/promo.php') ?>
    <!-- -->

    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->

    <!-- Modal -->
        <div class="hide">
            <div class="modal friends-block" id="friends">
                <div class="modal-header">узнать мнение<br/>друзей</div>
                <div class="modal-body">
                    <div class="modal-border">
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="#" class="modal-image">
                                    <img src="images/product01.jpg" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <div class="modal-text">
                                    <div class="modal-title">«Привет! Я выбираю себе велик. Как тебе такой вариант?»</div>
                                    <h4><a href="#">Maverick X29</a></h4>
                                    <ul class="product-info">
                                        <li>Рама: Сталь Hi-Ten</li>
                                        <li>Количество скоростей: 6</li>
                                        <li>Размер колес: 26"</li>
                                        <li>Тормоза: Ободные (V-Brake)</li>
                                        <li>Вес: 14,7 кг</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="btn-friends clearfix">
                        <li><a href="#" class="btn btn-sm btn-fb"><i class="fa fa-facebook" aria-hidden="true"></i> Отправить</a></li>
                        <li><a href="#" class="btn btn-sm btn-vk"><i class="fa fa-vk" aria-hidden="true"></i> Отправить</a></li>
                        <li><a href="#" class="btn btn-sm btn-tw"><i class="fa fa-twitter" aria-hidden="true"></i> Отправить</a></li>
                        <li><a href="#" class="btn btn-sm btn-ok"><i class="fa fa-odnoklassniki" aria-hidden="true"></i> Отправить</a></li>
                        <li><a href="#" class="btn btn-sm btn-gp"><i class="fa fa-google-plus" aria-hidden="true"></i> Отправить</a></li>
                    </ul>
                </div>
            </div>
        </div>
    <!-- -->

    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

</body>

</html>


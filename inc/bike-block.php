<section class="index-box-three">
    <div class="container">
        <div class="h1 text-center">Надёжность, качество,<br/>и собственное производство</div>
        <div class="bike">
            <div class="bike-inner">
                <img src="img/bike.png" alt="" class="img-responsive">
                <div class="bike-point">
                    <ul>
                        <li class="item1 active tab1">
                            <a href="#" data-target=".tab1"></a>
                            <div class="bike-text-item">
                                <div class="bike-text-title">1 год</div>
                                <p>гарантия на навесное оборудование</p>
                            </div>
                        </li>
                        <li class="item2 tab2">
                            <a href="#" data-target=".tab2"></a>
                            <div class="bike-text-item">
                                <div class="bike-text-title">Вкладка 2</div>
                                <p>гарантия на навесное оборудование</p>
                            </div>
                        </li>
                        <li class="item3 tab3">
                            <a href="#" data-target=".tab3"></a>
                            <div class="bike-text-item">
                                <div class="bike-text-title">Вкладка 3</div>
                                <p>гарантия на навесное оборудование</p>
                            </div>
                        </li>
                        <li class="item4 tab4">
                            <a href="#" data-target=".tab4"></a>
                            <div class="bike-text-item">
                                <div class="bike-text-title">Вкладка 4</div>
                                <p>гарантия на навесное оборудование</p>
                            </div>
                        </li>
                        <li class="item5 tab5">
                            <a href="#" data-target=".tab5"></a>
                            <div class="bike-text-item">
                                <div class="bike-text-title">Вкладка 5</div>
                                <p>гарантия на навесное оборудование</p>
                            </div>
                        </li>
                        <li class="item6 tab6">
                            <a href="#" data-target=".tab6"></a>
                            <div class="bike-text-item">
                                <div class="bike-text-title">Вкладка 6</div>
                                <p>гарантия на навесное оборудование</p>
                            </div>
                        </li>
                        <li class="item7 tab7">
                            <a href="#" data-target=".tab7"></a>
                            <div class="bike-text-item">
                                <div class="bike-text-title">Вкладка 7</div>
                                <p>гарантия на навесное оборудование</p>
                            </div>
                        </li>
                        <li class="item8 tab8">
                            <a href="#" data-target=".tab8"></a>
                            <div class="bike-text-item">
                                <div class="bike-text-title">Вкладка 8</div>
                                <p>гарантия на навесное оборудование</p>
                            </div>
                        </li>
                    </ul>
                    <div class="bike-slogan">#foryou</div>
                </div>
            </div>
            <div class="bike-text">
                <ul>
                    <li class="item1 tab1 active">
                        <div class="bike-text-title">1 год</div>
                        <p>гарантия на навесное оборудование</p>
                    </li>
                    <li class="item2 tab2">
                        <div class="bike-text-title">Вкладка 2</div>
                        <p>гарантия на навесное оборудование</p>
                    </li>
                    <li class="item3 tab3">
                        <div class="bike-text-title">Вкладка 3</div>
                        <p>гарантия на навесное оборудование</p>
                    </li>
                    <li class="item4 tab4">
                        <div class="bike-text-title">Вкладка 4</div>
                        <p>гарантия на навесное оборудование</p>
                    </li>
                    <li class="item5 tab5">
                        <div class="bike-text-title">Вкладка 5</div>
                        <p>гарантия на навесное оборудование</p>
                    </li>
                    <li class="item6 tab6">
                        <div class="bike-text-title">Вкладка 6</div>
                        <p>гарантия на навесное оборудование</p>
                    </li>
                    <li class="item7 tab7">
                        <div class="bike-text-title">Вкладка 7</div>
                        <p>гарантия на навесное оборудование</p>
                    </li>
                    <li class="item8 tab8">
                        <div class="bike-text-title">Вкладка 8</div>
                        <p>гарантия на навесное оборудование</p>
                    </li>
                </ul>
            </div>
            <ul class="bike-dots">
                <li class="tab1 active"><a href="#" data-target=".tab1"></a></li>
                <li class="tab2"><a href="#" data-target=".tab2"></a></li>
                <li class="tab3"><a href="#" data-target=".tab3"></a></li>
                <li class="tab4"><a href="#" data-target=".tab4"></a></li>
                <li class="tab5"><a href="#" data-target=".tab5"></a></li>
                <li class="tab6"><a href="#" data-target=".tab6"></a></li>
                <li class="tab7"><a href="#" data-target=".tab7"></a></li>
                <li class="tab8"><a href="#" data-target=".tab8"></a></li>
            </ul>
        </div>
        <div class="text">
            Мы берем на себя весь цикл производства велосипедов Maverick:
            от разработки дизайна новых моделей до конвейерной сборки
            на собственном предприятии в Калининграде.
        </div>
    </div>
</section>
// Navs

var navBar = $(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle(100);
        pull.toggleClass('open');
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 992 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
        if(w > 992) {
            $('.navbar-back').removeClass('active');
            $('.nav').removeClass('sub-open');
            $('.parents-nav').removeClass('open');
        }
    });

    $('.navbar-close').on('click', function(e) {
        e.preventDefault();
        menu.slideToggle(100);
        $(this).closest('.topnav').find('.navbar-back').removeClass('active');
        $(this).closest('.topnav').find('.nav').removeClass('sub-open');
        $(this).closest('.topnav').find('.parents-nav').removeClass('open');
    });


    $('.parents-nav > a').on('click', function(e) {
        e.preventDefault();
        var title = $(this).text();
        $('.navbar-name').text(title);
        $(this).closest('.navbar').find('.navbar-back').toggleClass('active');
        $(this).closest('.nav').toggleClass('sub-open');

        var submenu = $(this).closest('.parents-nav');

        if (submenu.hasClass("open")) {
            submenu.toggleClass('open');
        }
        else {
            $(this).closest('.topnav').find('.parents-nav').removeClass('open');
            submenu.toggleClass('open');
        }
    });

    $(document).click(function(event) {
        var w = $(window).width();

        if (w < 992 ) {

            if (menu.is(':hidden')){
            }

            else {
                if ($(event.target).closest(".topnav").length) return;
                menu.slideToggle(100);
                pull.toggleClass('open');
            }
        }
    });

    $('.navbar-back > span').on('click', function(e) {
        e.preventDefault();
        $('.navbar-name').text('Меню');
        $(this).closest('.topnav').find('.navbar-back').removeClass('active');
        $(this).closest('.topnav').find('.nav').removeClass('sub-open');
        $(this).closest('.topnav').find('.parents-nav').removeClass('open');
    });
});


// Изменение оформления категорий

var cat = $(function(){

    $('.cat-view li').click(function(e) {
        e.preventDefault();
        var sort = $(this).attr("data-target");

        console.log(sort);

        $(this).closest('.cat-view').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        if (sort == 1) {
            $('.showcase').removeClass('showcase-table');
        }

        if (sort == 2) {
            $('.showcase').addClass('showcase-table');
        }
    });
});

// Фильтр


$("input[type='radio']").ionCheckRadio();
$("input[type='checkbox']").ionCheckRadio();


var panel = $(function(){

    var btn = $('.btn-switch');
    var content = $(btn.attr("data-target"));

    $(btn).on('click', function(e) {
        e.preventDefault();
        content.toggleClass('open');
        btn.toggleClass('open');
    });


    $(window).resize(function(){
        var w = $(window).width();
        if(w > 992 && content.is('.open')) {
            content.removeClass('open');
        }
    });

    $('.panel-switch-hide').click(function(e) {
        e.preventDefault();
        content.toggleClass('open');
    });

    content.click(function(event) {

        if (content.is('.open')){
            if ($(event.target).closest(".panel-switch").length) return;
            content.toggleClass('open');
            event.stopPropagation();
        }
    });

});





// Пункты фильтра

$('.filter-name').click(function(e) {
    e.preventDefault();
    var filter = $(this).closest('.filter-item');
    filter.toggleClass('open');
});

// выпадающий список


function DropDown(el) {
    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.opts = this.dd.find('.list-dropdown li');
    this.val = '';
    this.index = -1;
    this.initEvents();
}

DropDown.prototype = {
    initEvents : function() {
        var obj = this;

        obj.dd.on('click', function(event){
            $(this).toggleClass('active');
            return false;
        });

        obj.opts.on('click',function(){
            var opt = $(this);
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text(obj.val);
        });
    },
    getValue : function() {
        return this.val;
    },
    getIndex : function() {
        return this.index;
    }
}

$(function() {

    var dd = new DropDown( $('#dd') );
    var dd2 = new DropDown( $('#dd2') );

    $(document).click(function() {
        // all dropdowns
        $('.wrapper-dropdown').removeClass('active');
    });

});



// tabs

$('.tabs-nav li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.tabs');

    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});


$('.btn-detail').click(function(e) {
    e.preventDefault();
    var box = $(this).closest('.product-detail');

    $(this).hide();
    box.find('.product-detail-hide').show();

});


$('.slider').slick({
    dots: true,
    arrows: false,
    loop: true,
    slidesToShow: 1,
    slidesToScroll: 1
});


$('.slider-like').slick({
    dots: true,
    arrows: false,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [

        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        }
    ]
});


$('.staff-slider').slick({
    dots: true,
    arrows: false,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [

        {
            breakpoint: 992,
            settings: {
                dots: false
            }
        }
    ]
});

$('.staff-nav').click(function(){
    $('.staff-slider').slick('slickNext');
});


$('.about-slider').slick({
    dots: true,
    arrows: false,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [

        {
            breakpoint: 992,
            settings: {
                dots: true
            }
        }
    ]
});


$('.about-nav .item3 a').click(function(){
    $('.about-slider').slick('slickNext');
});


var partners = $('.partners-nav li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.partners');

    $(this).closest('.partners-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.partners-item').removeClass('active');
    box.find(tab).addClass('active');
});

// Слайдеры страницы Дилерам

$('.dealers-features').slick({
    dots: true,
    arrows: false,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [

        {
            breakpoint: 992,
            settings: {
                dots: false,
                slidesToShow: 4
            }
        }
    ]
});

$('.dealers-coin').slick({
    dots: true,
    arrows: false,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [

        {
            breakpoint: 992,
            settings: {
                dots: false,
                slidesToShow: 3
            }
        }
    ]
});

$('.dealers-money').slick({
    dots: true,
    arrows: false,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [

        {
            breakpoint: 992,
            settings: {
                dots: false,
                slidesToShow: 2
            }
        }
    ]
});


$('.risk').slick({
    dots: true,
    arrows: false,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [

        {
            breakpoint: 992,
            settings: {
                dots: false,
                slidesToShow: 3
            }
        }
    ]
});


$('.who').slick({
    dots: true,
    arrows: false,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [

        {
            breakpoint: 992,
            settings: {
                dots: false,
                slidesToShow: 4
            }
        }
    ]
});

$('.news-block-slider').slick({
    dots: true,
    arrows: false,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [

        {
            breakpoint: 992,
            settings: {
                dots: false,
                slidesToShow: 4
            }
        }
    ]
});


$('.account-action-slider').slick({
    dots: true,
    arrows: false,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [

        {
            breakpoint: 992,
            settings: {
                dots: false,
                vertical: true,
                slidesToShow: 3
            }
        }
    ]
});



$('.people').slick({
    dots: true,
    arrows: false,
    loop: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [

        {
            breakpoint: 992,
            settings: {
                dots: false,
                slidesToShow: 4
            }
        }
    ]
});


$('.post-image-slider').slick({
    dots: true,
    arrows: false,
    loop: true,
    autoplay: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1
});



// Модальные окна

$(".btn-modal").fancybox({
    'padding'    : 0,
    'tpl'        : {
        closeBtn : '<a title="Close" class="btn-close" href="javascript:;"></a>',
        wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner scroll-inner"></div></div></div></div>'
    }
});


// Велосипед на главной

$('.bike-point li a, .bike-dots li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.bike');

    box.find('li').removeClass('active');
    box.find(tab).addClass('active');

});


$(function () {
    $('.select-style').ikSelect({
        autoWidth: false,
        ddFullWidth: false,
        dynamicWidth: false,
        equalWidths: true,
        extractLink: false,
        linkCustomClass: '',
        ddCustomClass: '',
        filter: false,
        ddMaxHeight: 300,
        customClass: 'select-main'
    });
});


// Устройство велосипеда

$('.construction-filter li a').click(function(e) {
    e.preventDefault();
    var detail = $($(this).attr("data-target"));
    var box = $(this).closest('.construction');

    $(this).closest('.construction-filter').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.construction-image li').removeClass('active');
    box.find('.construction-description li').removeClass('active');
    box.find(detail).addClass('active');
});


// Фильтр карты

var mapFilter =  $(function(){

    var btnMap = $('.map-toggle');
    var fltMap = $(btnMap.attr("data-target"));

    $(btnMap).on('click', function(e) {
        e.preventDefault();
        fltMap.toggleClass('open');
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 992 && fltMap.is(':hidden')) {
            fltMap.removeAttr('style');
        }
    });

    $('.map-filter-hide').click(function(e) {
        e.preventDefault();
        fltMap.toggleClass('open');
    });

    $(document).click(function(event) {
        if (fltMap.is('.open')){
            if ($(event.target).closest(".map-xs").length) return;
            fltMap.toggleClass('open');
            event.stopPropagation();
        }
    });

});



/// Всплывающие подсказки

$( document ).ready( function()
{
    var targets = $( '[rel~=tooltip]' ),
        target  = false,
        tooltip = false,
        title   = false;

    targets.bind( 'mouseenter', function()
    {
        target  = $( this );
        tip     = target.attr( 'title' );
        tooltip = $( '<div id="tooltip"></div>' );

        if( !tip || tip == '' )
            return false;

        target.removeAttr( 'title' );
        tooltip.css( 'opacity', 0 )
            .html( tip )
            .appendTo( 'body' );

        var init_tooltip = function()
        {
            if( $( window ).width() < tooltip.outerWidth() * 1.5 )
                tooltip.css( 'max-width', $( window ).width() / 2 );
            else
                tooltip.css( 'max-width', 200 );

            var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
                pos_top  = target.offset().top - tooltip.outerHeight() - 25;

            if( pos_left < 0 )
            {
                pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                tooltip.addClass( 'left' );
            }
            else
                tooltip.removeClass( 'left' );

            if( pos_left + tooltip.outerWidth() > $( window ).width() )
            {
                pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                tooltip.addClass( 'right' );
            }
            else
                tooltip.removeClass( 'right' );

            if( pos_top < 0 )
            {
                var pos_top  = target.offset().top + target.outerHeight();
                tooltip.addClass( 'top' );
            }
            else
                tooltip.removeClass( 'top' );

            tooltip.css( { left: pos_left, top: pos_top } )
                .animate( { top: '+=7', opacity: 1 }, 50 );
        };

        init_tooltip();
        $( window ).resize( init_tooltip );

        var remove_tooltip = function()
        {
            tooltip.animate( { top: '-=7', opacity: 0 }, 50, function()
            {
                $( this ).remove();
            });

            target.attr( 'title', tip );
        };

        target.bind( 'mouseleave', remove_tooltip );
        tooltip.bind( 'click', remove_tooltip );
    });
});


// Список представительств

$('.branch-result-name').click(function(e) {
    e.preventDefault();
    var list = $(this).closest('ul');
    list.toggleClass('open');
});
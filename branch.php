<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->


    <section class="branch pb50">
        <div class="container">
            <a class="btn-toggle btn-switch svg-responsive" data-target=".panel-switch-inner">
                <img src="img/btn-toggle.svg" alt="">
            </a>

            <div class="panel-switch-inner">
                <div class="map-filter panel-switch">
                    <div class="map-filter-content">
                        <span class="map-filter-hide panel-switch-hide"></span>
                        <div class="map-filter-search">
                            <form class="form">
                                <input type="text" class="form-search" placeholder="Искать по названию">
                            </form>
                        </div>
                        <div class="map-filter-result">
                            <div class="result-text">
                                <div class="clearfix mb50">
                                    <div class="form-checkbox">
                                        <label>
                                            <span class="icr-text">Интернет-магазины</span>
                                            <input type="checkbox" name="f2" value="2">
                                        </label>
                                    </div>
                                    <div class="form-checkbox">
                                        <label>
                                            <span class="icr-text">Магазины</span>
                                            <input type="checkbox" name="f2" value="2">
                                        </label>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-md">Показать</button>
                            </div>
                            <div class="result-none">Мы не смогли найти<br/>такой регион</div>
                            <div class="result-list hide">
                                <ul>
                                    <li>Набережные Челны</li>
                                    <li>Нижневартовск</li>
                                    <li>Нижний Тагил</li>
                                    <li>Новокузнецк</li>
                                    <li>Новокуйбышевск</li>
                                    <li>Новочеркасск</li>
                                    <li>Ноябрьск</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="map-content">
                <div class="h1">Где купить<br/></div>
                <div class="map-list hidden-xs">
                    <div class="form-group">
                        <select class="form-control select-style" name="region">
                            <option>По всей России</option>
                            <option>Москва</option>
                            <option>Санкт-Петербург</option>
                            <option>Тамбов</option>
                            <option>Волгоград</option>
                            <option>Орел</option>
                        </select>
                    </div>
                    <div class="branch-type clearfix">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-checkbox">
                                    <label>
                                        <span class="icr-text">Интернет-магазины</span>
                                        <input type="checkbox" name="f2" value="2">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-checkbox">
                                    <label>
                                        <span class="icr-text">Магазины</span>
                                        <input type="checkbox" name="f2" value="2">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="wmap"></div>
        <div class="container">
            <ul class="branch-title">
                <li class="branch-title-legend">всего по России:</li>
                <li class="branch-title-shop"><span>9 магазинов</span></li>
                <li class="branch-title-online"><span>и 6 интернет-магазинов</span></li>
            </ul>
            <div class="branch-text">
                <p><a class="btn-modal" href="#city">Выберите свой город</a> , чтобы увидеть список ближайших дилеров</p>
            </div>
        </div>
    </section>
    <!-- Footer Banner -->
    <?php include('inc/promo.php') ?>
    <!-- -->

    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->



    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    <script src="js/service.map.js"></script>

    <!-- Modal -->
    <div class="hide">
        <div class="modal modal-city" id="city">
            <div class="modal-header">Ваш город</div>
            <div class="modal-body">
                <h2>Ваш город</h2>
                <p>Выберите ближайший город (регион), в котором хотите найти дилеров</p>
                <div class="row">

                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <h5>Б</h5>
                        <ul class="city-group">
                            <li><a href="#">Барнаул</a></li>
                            <li><a href="#">Белгород</a></li>
                        </ul>
                        <h5>В</h5>
                        <ul class="city-group">
                            <li><a href="#">Великий Новгород</a></li>
                            <li><a href="#">Владивосток</a></li>
                            <li><a href="#">Владимир</a></li>
                            <li><a href="#">Вологда</a></li>
                            <li><a href="#">Воркута</a></li>
                            <li><a href="#">Всеволожск</a></li>
                            <li><a href="#">Выборг</a></li>
                        </ul>
                        <h5>Г</h5>
                        <ul class="city-group">
                            <li><a href="#">Гатчина</a></li>
                            <li><a href="#">Горно-Алтайск</a></li>
                        </ul>
                        <h5>И</h5>
                        <ul class="city-group">
                            <li><a href="#">Иваново</a></li>
                            <li><a href="#">Ижевск</a></li>
                            <li><a href="#">Иркутск</a></li>
                        </ul>
                    </div>

                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <h5>К</h5>
                        <ul class="city-group">
                            <li><a href="#">Калуга</a></li>
                            <li><a href="#">Кемерово</a></li>
                            <li><a href="#">Кингисепп</a></li>
                            <li><a href="#">Кириши</a></li>
                            <li><a href="#">Колпино</a></li>
                            <li><a href="#">Кострома</a></li>
                            <li><a href="#">Краснодар</a></li>
                            <li><a href="#">Красноярск</a></li>
                            <li><a href="#">Курск</a></li>
                        </ul>
                        <h5>Л</h5>
                        <ul class="city-group">
                            <li><a href="#">Липецк</a></li>
                            <li><a href="#">Люберцы</a></li>


                        </ul>
                        <h5>М</h5>
                        <ul class="city-group">
                            <li><a href="#">Мурманск</a></li>
                            <li><a href="#">Мытищи</a></li>
                        </ul>
                    </div>

                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <h5>Н</h5>
                        <ul class="city-group">
                            <li><a href="#">Набережные Челны</a></li>
                            <li><a href="#">Нижневартовск</a></li>
                            <li><a href="#">Нижний Тагил</a></li>
                            <li><a href="#">Новокузнецк</a></li>
                            <li><a href="#">Новокуйбышевск</a></li>
                            <li><a href="#">Новочеркасск</a></li>
                            <li><a href="#">Ноябрьск</a></li>
                        </ul>
                        <h5>П</h5>
                        <ul class="city-group">
                            <li><a href="#">Пенза</a></li>
                            <li><a href="#">Пермь</a></li>
                            <li><a href="#">Петрозаводск</a></li>
                            <li><a href="#">Подольск</a></li>
                            <li><a href="#">Псков</a></li>
                            <li><a href="#">Пушкин</a></li>
                            <li><a href="#">Пятигорск</a></li>
                        </ul>
                    </div>

                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <h5>Р</h5>
                        <ul class="city-group">
                            <li><a href="#">Рязань</a></li>
                        </ul>
                        <h5>С</h5>
                        <ul class="city-group">
                            <li><a href="#">Саратов</a></li>
                            <li><a href="#">Сергиев Посад</a></li>
                            <li><a href="#">Серпухов</a></li>
                            <li><a href="#">Смоленск</a></li>
                            <li><a href="#">Сочи</a></li>
                            <li><a href="#">Ставрополь</a></li>
                            <li><a href="#">Сургут</a></li>
                        </ul>
                        <h5>П</h5>
                        <ul class="city-group">
                            <li><a href="#">Тамбов</a></li>
                            <li><a href="#">Тверь</a></li>
                            <li><a href="#">Тихвин</a></li>
                            <li><a href="#">Тольятти</a></li>
                            <li><a href="#">Томск</a></li>
                            <li><a href="#">Тула</a></li>
                            <li><a href="#">Тюмень</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- -->

    </body>
</html>

<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->

    <section class="main">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="#">Главная</a></li>
            </ul>
            <h1>Новости</h1>
            <div class="news clearfix">
                <span class="news-toggle btn-switch" data-target=".panel-switch-inner">
                    <img src="img/btn-toggle.svg" alt="">
                </span>

                <div class="news-body">

                    <article class="post">
                        <div class="post-image">
                            <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                        </div>
                        <span class="post-date">258 мая 2016</span>
                        <ul class="post-tags">
                            <li><a href="#">акция</a></li>
                            <li><a href="#">снижение цены</a></li>
                        </ul>
                        <h2><a href="#">Велошколы для пенсионеров в Санкт-Петербурге</a></h2>
                        <div class="post-preview">
                            Почти в каждом районе Северной столицы заработали специальные экологические кружки для пенсионеров – любой желающий может бесплатно получить уроки езды на велосипеде! Инструкторы объясняют слушателям правила...
                        </div>
                    </article>

                    <article class="post">
                        <div class="post-image">
                            <img src="images/action-img-large.jpg" alt="" class="img-responsive">
                        </div>
                        <span class="post-date">258 мая 2016</span>
                        <ul class="post-tags">
                            <li><a href="#">акция</a></li>
                            <li><a href="#">снижение цены</a></li>
                        </ul>
                        <h2><a href="#">Велошколы для пенсионеров в Санкт-Петербурге</a></h2>
                        <div class="post-preview">
                            Почти в каждом районе Северной столицы заработали специальные экологические кружки для пенсионеров – любой желающий может бесплатно получить уроки езды на велосипеде! Инструкторы объясняют слушателям правила...
                        </div>
                    </article>

                    <a href="#" class="btn-view">Показать еще</a>
                    <ul class="pagination">
                        <li class="begin"><a href="#">В начало</a></li>
                        <li class="prev"><a href="#">←</a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li class="next"><a href="#">→</a></li>
                    </ul>

                </div>

                <div class="news-side">
                    <div class="panel-switch-inner">
                        <div class="news-filter panel-switch">
                            <span class="news-filter-close panel-switch-hide"></span>
                            <div class="news-filter-title">Показать новости в рубриках</div>
                            <ul class="tags">
                                <li>
                                    <label class="checked">
                                        <span class="icr-text">Акция</span>
                                        <input type="checkbox" name="n1" value="1" checked>
                                    </label>
                                </li>
                                <li>
                                    <label class="checked">
                                        <span class="icr-text">Новинка</span>
                                        <input type="checkbox" name="n1" value="1" checked>
                                    </label>
                                </li>
                                <li>
                                    <label class="checked">
                                        <span class="icr-text">Снижение цены</span>
                                        <input type="checkbox" name="n1" value="1" checked>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <span class="icr-text">Жизнь компании</span>
                                        <input type="checkbox" name="n1" value="1">
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <span class="icr-text">Велоспорт</span>
                                        <input type="checkbox" name="n1" value="1">
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <span class="icr-text">Игорь</span>
                                        <input type="checkbox" name="n1" value="1">
                                    </label>
                                </li>
                            </ul>
                            <div class="news-filter-button">
                                <button type="submit" class="btn">Показать</button>
                            </div>
                            <a href="#" class="news-filter-all">показать все новости</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer Banner -->
    <?php include('inc/promo.php') ?>
    <!-- -->

    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->

    <!-- Modal -->
    <div class="hide">
        <div class="modal modal-sm" id="question">
            <div class="modal-header">Поддержка клиента</div>
            <div class="modal-body">
                <h3>Поддержка клиента</h3>
                <form class="form">
                    <div class="form-group">
                        <input type="text" name="" class="form-control" placeholder="Ваше имя">
                    </div>
                    <div class="form-group">
                        <input type="text" name="" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <textarea name="message" class="form-control" placeholder="Текст сообщения" rows="4"></textarea>
                    </div>
                    <div class="pb20"></div>
                    <button type="submit" class="btn btn-send">Отправить</button>
                </form>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    </body>
</html>

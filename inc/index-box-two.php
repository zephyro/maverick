<section class="index-box-two">
    <div class="container">
        <div class="text-block">
            <h2>Полные каталоги<br/>велосипедов "MAVERICK"</h2>
            <div class="download-box">
                <div class="row">
                    <div class="col-md-7">
                        <a href="#" class="btn">Скачать каталог 2016</a>
                    </div>
                    <div class="col-md-5">
                        <div class="help-text">PDF — 3,4Мб</div>
                    </div>
                </div>
            </div>
            <a class="green-link" href="#">Скачать каталог 2015 <br/>PDF — 4,5Мб</a>
        </div>
        <div class="img-block">
            <img src="img/dealer-order-img.png" alt="" class="img-responsive">
        </div>
    </div>
</section>
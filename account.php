<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->

    <div class="account-header">
        <div class="container">
            <div class="t-row">
                <div class="t-col">Иван Олегович Тинькофф</div>
            </div>
            <span class="account-toggle btn-switch svg-responsive" data-target=".panel-switch-inner">
                <img src="img/account-toggle.svg" alt="">
            </span>
        </div>
    </div>

    <section class="account">
        <div class="container">
            <div class="clearfix">

                <div class="account-body">
                    <h1>Разместить заказ очень просто</h1>

                    <div class="account-content">
                        <div class="step-title">
                            <div class="step-num">1</div>
                            <span>Скачайте бланк с актуальным остатком склада и сформируйтесвой заказ</span>
                        </div>
                        <div class="blue-box">
                            <p>Сформируйте заказ в бланке, поставив нужноеколичество единиц товара напротив каждой модели</p>
                            <a href="#" class="btn">Скачать бланк</a>
                        </div>

                        <div class="step-title">
                            <div class="step-num">2</div>
                            <span>Прикрепите сформированныйфайл заказа</span>
                        </div>

                        <div class="blue-box">
                            <form class="form">
                                <div class="form-group">
                                    <div class="file-form">
                                        <p>Перетащите файл сюда<br/>или</p>
                                        <a href="#" class="btn-blue btn-blue-md">Прикрепить файл</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Добавьте комментарий"></textarea>
                                </div>
                                <button type="submit" class="btn btn-send">Разместить заказ</button>
                            </form>
                        </div>

                        <div class="account-content-footer">
                            <p>Возникли сложности с заказом?</p>
                            <p><a href="#question" class="btn-modal">Задайте нам вопрос и мы свяжемся с вами</a></p>
                        </div>
                    </div>
                </div>

                <div class="account-side">
                    <div class="panel-switch-inner">
                        <div class="account-profile panel-switch">
                            <div class="ap-header">
                                <div class="ap-first-name">Иван Олегович</div>
                                <div class="ap-last-name">Тинькофф</div>
                                <div class="ap-company">ИП Тинькофф</div>
                            </div>

                            <div class="ap-body">
                                <ul class="ap-nav">
                                    <li class="item1"><a href="#">Получить доступный остаток</a></li>
                                    <li class="item2"><a href="#">Скачать каталог MAVERICK</a></li>
                                    <li class="item3"><a href="#">Поддержка клиента</a></li>
                                </ul>
                                <div class="ap-divider"></div>
                                <ul class="ap-nav">
                                    <li class="item4"><a href="#">Личные данные</a></li>
                                    <li class="item5"><a href="#">Выход</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="account-action">
                        <h3>Акции</h3>

                        <div class="account-action-slider">

                            <div class="account-action-item clearfix">
                                <div class="account-action-image">
                                    <img src="images/action-img.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="account-action-text">
                                    <h4><a href="#">Скидка на серию Х</a></h4>
                                    <p>Скидка на все  двухподвесы серии Х</p>
                                    <div class="account-action-date">20 мая 2016</div>
                                </div>
                            </div>

                            <div class="account-action-item clearfix">
                                <div class="account-action-image">
                                    <img src="images/action-img.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="account-action-text">
                                    <h4><a href="#">Изменение цен</a></h4>
                                    <p>Внимание! 05.06.206 изменились цены на модели в категориях “дорожные” и “подростковые”</p>
                                    <div class="account-action-date">20 мая 2016</div>
                                </div>
                            </div>

                            <div class="account-action-item clearfix">
                                <div class="account-action-image">
                                    <img src="images/action-img.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="account-action-text">
                                    <h4><a href="#">Новая модель GTR</a></h4>
                                    <p>Рады представить вам новую модель GTR</p>
                                    <div class="account-action-date">20 мая 2016</div>
                                </div>
                            </div>
                        </div>

                        <a href="#" class="btn-blue btn-blue-md">Смотреть все акции</a>
                    </div>

                </div>
            </div>
        </div>
    </section>


    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->

    <!-- Modal -->
    <div class="hide">
        <div class="modal modal-sm" id="question">
            <div class="modal-header">Поддержка клиента</div>
            <div class="modal-body">
                <h3>Поддержка клиента</h3>
                <form class="form">
                    <div class="form-group">
                        <input type="text" name="" class="form-control" placeholder="Ваше имя">
                    </div>
                    <div class="form-group">
                        <input type="text" name="" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <textarea name="message" class="form-control" placeholder="Текст сообщения" rows="4"></textarea>
                    </div>
                    <div class="pb20"></div>
                    <button type="submit" class="btn btn-send">Отправить</button>
                </form>
            </div>
        </div>
    </div>
    <!-- -->

    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    </body>
</html>

<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->


    <section class="contact">
        <div class="container">
            <div class="content-top">
                <h1>Контакты</h1>
            </div>
        </div>
        <div class="contact-map">
            <div id="contact-map">

            </div>
        </div>
        <div class="container">
            <div class="contacts tabs">
                <ul class="tabs-nav clearfix">
                    <li class="active"><a href="#" data-target=".tab1">Офис</a></li>
                    <li><a href="#" data-target=".tab2">Производство</a></li>
                    <li><a href="#" data-target=".tab3">Склад</a></li>
                </ul>

                <div class="tabs-item tab1 active">
                    <div class="row">
                        <div class="col-md-4 col-lg-6">
                            <div class="address">614000, Россия, г. Москва, ул. Академика Королева, 60<br/>
                                <a href="tel:+7 (950) 202-22-22">+7 (950) 202-22-22</a> , <a href="tel:+7 (909) 999-99-99">+7 (909) 999-99-99</a></div>
                        </div>
                        <div class="col-md-4 col-lg-3">
                            <div class="worktime">
                                Ежедневно с 9:00 до 21:00.<br/>
                                Выходные: Сб, Вс.
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-3">
                            <div class="email">
                                <a href="mailto:support@maverick.ru">support@maverick.ru</a><br/>
                                <a href="mailto:help@maverick.ru">help@maverick.ru</a>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="tabs-item tab2">
                    <div class="row">
                        <div class="col-md-4 col-lg-6">
                            <div class="address">614000, Россия, г. Москва, ул. Профсоюзаня, 60<br/>
                                <a href="tel:+7 (950) 202-22-22">+7 (950) 202-22-22</a> , <a href="tel:+7 (909) 999-99-99">+7 (909) 999-99-99</a></div>
                        </div>
                        <div class="col-md-4 col-lg-3">
                            <div class="worktime">
                                Ежедневно с 9:00 до 21:00.<br/>
                                Выходные: Сб, Вс.
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-3">
                            <div class="email">
                                <a href="mailto:support@maverick.ru">support@maverick.ru</a><br/>
                                <a href="mailto:help@maverick.ru">help@maverick.ru</a>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="tabs-item tab3">
                    <div class="row">
                        <div class="col-md-4 col-lg-6">
                            <div class="address">614000, Россия, г. Москва, Ленинский проспект, 14<br/>
                                <a href="tel:+7 (950) 202-22-22">+7 (950) 202-22-22</a> , <a href="tel:+7 (909) 999-99-99">+7 (909) 999-99-99</a></div>
                        </div>
                        <div class="col-md-4 col-lg-3">
                            <div class="worktime">
                                Ежедневно с 9:00 до 21:00.<br/>
                                Выходные: Сб, Вс.
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-3">
                            <div class="email">
                                <a href="mailto:support@maverick.ru">support@maverick.ru</a><br/>
                                <a href="mailto:help@maverick.ru">help@maverick.ru</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="main">
        <div class="container">
            <div class="people">

                <div class="people-item">
                    <div class="people-image">
                        <img src="images/contacts/user01.png" alt="" class="img-responsive">
                    </div>
                    <div class="people-name">Роман<br/>Смоляков</div>
                    <div class="people-position">Директор</div>
                    <a href="tel:+7 (912) 212-12-12" class="tel">+7 (912) 212-12-12</a>
                    <br/>
                    <a href="mailto:smolyakov@gmail.com">smolyakov@gmail.com</a>
                </div>

                <div class="people-item">
                    <div class="people-image">
                        <img src="images/contacts/user02.png" alt="" class="img-responsive">
                    </div>
                    <div class="people-name">Александр<br/>Кожевников</div>
                    <div class="people-position">Менеджер отдела оптовых продаж</div>
                    <a href="tel:+7 (912) 212-12-12" class="tel">+7 (912) 212-12-12</a>
                    <br/>
                    <a href="mailto:smolyakov@gmail.com">smolyakov@gmail.com</a>
                </div>

                <div class="people-item">
                    <div class="people-image">
                        <img src="images/contacts/user03.png" alt="" class="img-responsive">
                    </div>
                    <div class="people-name">Дмитрий<br/>Морозов</div>
                    <div class="people-position">Менеджер производственного отдела</div>
                    <a href="tel:+7 (912) 212-12-12" class="tel">+7 (912) 212-12-12</a>
                    <br/>
                    <a href="mailto:smolyakov@gmail.com">smolyakov@gmail.com</a>
                </div>

                <div class="people-item">
                    <div class="people-image">
                        <img src="images/contacts/user04.png" alt="" class="img-responsive">
                    </div>
                    <div class="people-name">Евгений<br/>Епишин</div>
                    <div class="people-position">Смотрящий</div>
                    <a href="tel:+7 (912) 212-12-12" class="tel">+7 (912) 212-12-12</a>
                    <br/>
                    <a href="mailto:smolyakov@gmail.com">smolyakov@gmail.com</a>
                </div>

            </div>
        </div>
    </section>

    <!-- Footer Banner -->
    <?php include('inc/promo.php') ?>
    <!-- -->

    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->



    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    <script src="js/contact.map.js"></script>

    </body>
</html>

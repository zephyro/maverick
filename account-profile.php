<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->

    <div class="account-header">
        <div class="container">
            <div class="t-row">
                <div class="t-col">Иван Олегович Тинькофф</div>
            </div>
        </div>
    </div>

    <section class="account-reg">
        <div class="container">
            <h1>Личные данные</h1>
            <div class="account-form-container">
                <form class="form">
                    <div class="account-form">

                        <fieldset class="fieldset">
                            <legend>Личные данные</legend>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="form-label"><span>Имя</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="n1" placeholder="Введите ваше имя">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="form-label"><span>Фамилия</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="n1" placeholder="Введите  фамилию">
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="fieldset">
                            <legend>Данные дилера</legend>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="form-label"><span>Ваш город</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="n1" placeholder="Введите название города">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="form-label"><span>ИНН</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="n1" placeholder="1234 567 890 1234 56">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="form-label"><span>Название магазина</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="n1" placeholder="Введите название магазина">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3 col-sm-offset-3">
                                        <div class="form-checkbox">
                                            <label>
                                                <span class="icr-text">Магазин</span>
                                                <input type="checkbox" name="f2" value="2">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-checkbox">
                                            <label>
                                                <span class="icr-text">Интернет-магазин</span>
                                                <input type="checkbox" name="f2" value="2">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="form-label"><span>Телефон 1</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="n1" placeholder="8 888 888 88 88">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="form-label"><span>Телефон 2</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="n1" placeholder="8 888 888 88 88 (необязательно)">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="form-label"><span>Адрес</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="n1" placeholder="Введите адрес магазина">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="form-label"><span>Сайт</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="n1" placeholder="Введите адрес сайта (если есть)">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="form-label"><span>Сервис</span></label>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-radio">
                                            <label>
                                                <span class="icr-text">Да</span>
                                                <input type="radio" name="r1" value="1">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-radio">
                                            <label>
                                                <span class="icr-text">Нет</span>
                                                <input type="radio" name="r1" value="2">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>


                        <fieldset class="fieldset">
                            <legend>Данные для входа</legend>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="form-label"><span>Логин</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="n1" placeholder="58455485584584825">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <a href="#choice-pass" class="btn-modal">Изменить пароль</a>
                            </div>
                        </fieldset>

                    </div>
                    <div class="account-submit">
                        <button type="submit" class="btn">Зарегистрироваться</button>
                    </div>

                    <!-- УДАЛИТЬ НА БОЕВОМ СЕРВЕРЕ (добавлено для демонстрации работы окна -->
                    <div class="text-center">
                        <br/>
                        <a href="#choice-pass" class="btn-modal">Демонстрация работы всплывашки</a>
                    </div>
                    <!-- -->
                </form>
            </div>
        </div>
    </section>


    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->

    <!-- Modal -->
    <div class="hide">
        <div class="modal modal-sm" id="choice-pass">
            <div class="modal-header">Восстановить пароль</div>
            <div class="modal-body">
                <h3>Восстановить пароль</h3>
                <form class="form">
                    <div class="form-group">
                        <input type="text" name="" class="form-control" placeholder="E-mail">
                    </div>
                    <p class="pb20">На ваш email будет отправлена ссылка с новым паролем. Позже вы сможете поменять егов личном кабинете</p>
                    <button type="submit" class="btn">Восстановить</button>
                </form>
            </div>
        </div>
    </div>
    <!-- -->

    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    </body>
</html>

<!doctype html>
<html class="no-js" lang="ru">

<head>
    <?php include('inc/head.php') ?>
</head>

    <body>

    <!-- Navigation -->
    <?php include('inc/topnav.php') ?>
    <!-- -->

    <header class="header-dealer">
        <div class="container">
            <div class="h1">Велосипеды оптом напрямую от производителя с маржинальностью до 70%</div>

            <div class="dealer-form">
                <div class="hidden-xs">
                    <div class="dealer-form-header">Заявка партнёра</div>
                    <form class="form ">
                        <ul class="form-row clearfix">
                            <li>
                                <div class="form-group">
                                    <input type="text" name="" class="form-control" placeholder="Ваше имя">
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <input type="text" name="" class="form-control" placeholder="Название компании">
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <input type="text" name="" class="form-control" placeholder="Телефон">
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <input type="text" name="" class="form-control" placeholder="email">
                                </div>
                            </li>
                            <li>
                                <button type="submit" class="btn btn-md btn-send">Получить прайс и каталог</button>
                            </li>
                        </ul>
                    </form>
                </div>
                <a href="#dealers-form-modal" class="btn btn-md btn-modal hidden-sm hidden-md hidden-lg">Получить прайс и каталог</a>
            </div>

            <ul class="dealer-advantage clearfix">
                <li>
                    <div class="legend">Минимальный закуп</div>
                    <div class="value">От 100 000 рублей</div>
                </li>
                <li>
                    <div class="legend">В 5 раз надежнее аналогов</div>
                    <div class="value"> 0,1% рекламаций</div>
                </li>
                <li>
                    <div class="legend">Рост продаж из сезона в сезон</div>
                    <div class="value">В 1,5 раза</div>
                </li>
            </ul>
        </div>
    </header>

    <section class="section-dealer-one">
        <div class="container">
            <div class="h1 text-center padding-md">Велосипеды MavericK — это</div>
            <div class="dealers-features">
                <div class="features-item">
                    <div class="features-image">
                        <img src="img/dealers_features_01.png" alt="" class="img-responsive">
                    </div>
                    <div class="features-text">Работа напрямую<br/>с производителем</div>
                </div>
                <div class="features-item">
                    <div class="features-image">
                        <img src="img/dealers_features_02.png" alt="" class="img-responsive">
                    </div>
                    <div class="features-text">Экономия при сборке:<br/>трансмиссия и тормоза<br/>уже настроены на заводе</div>
                </div>
                <div class="features-item">
                    <div class="features-image">
                        <img src="img/dealers_features_03.png" alt="" class="img-responsive">
                    </div>
                    <div class="features-text">От 48 до 70%<br/>— ваша маржа</div>
                </div>
                <div class="features-item">
                    <div class="features-image">
                        <img src="img/dealers_features_04.png" alt="" class="img-responsive">
                    </div>
                    <div class="features-text">Выгода клиентам:<br/>крылья в комплекте<br/>с каждым велосипедом</div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-dealer-two">
        <div class="container">
            <div class="h1 text-center">Сколько вы заработаете<br/>с велосипедами «Maverick»</div>

            <div class="dealers-coin">
                <div class="coin-item">
                    <div class="coin-item-inner">
                        <div class="coin-header item1">
                            <div class="coin-value">48%</div>
                            <div class="coin-legend">маржа в сезон</div>
                        </div>
                        <p>средняя маржа партнеровпри заказе велосипедов Maverick в сезон</p>
                    </div>
                </div>
                <div class="coin-item">
                    <div class="coin-item-inner">
                        <div class="coin-header item2">
                            <div class="coin-value">62%</div>
                            <div class="coin-legend">маржа с предзаказа</div>
                        </div>
                        <p>средняя маржа партнеров, которые вступают в предзаказ</p>
                    </div>
                </div>
                <div class="coin-item">
                    <div class="coin-item-inner">
                        <div class="coin-header item3">
                            <div class="coin-value">70%</div>
                            <div class="coin-legend">маржа при раннем заказе</div>
                        </div>
                        <p>максимальная на текущий момент маржа партнеров при раннем заказе крупной партии и 100% предоплате</p>
                    </div>
                </div>
            </div>

            <div class="dealers-money">
                <div class="money-item">
                    <div class="money-item-inner">
                        <div class="money-header item1">
                            <div class="money-value">100 000 $</div>
                            <div class="money-legend">за сезон в небольших городах</div>
                        </div>
                        <p>текущий объем продаж велосипедов Maverick в небольших городах, например, в Пензе, Белгороде, Ростове-на-Дону</p>
                    </div>
                </div>
                <div class="money-item">
                    <div class="money-item-inner">
                        <div class="money-header item2">
                            <div class="money-value">400 000 $</div>
                            <div class="money-legend">за в сезон в Москве и Питере</div>
                        </div>
                        <p>текущий объем продаж велосипедов Maverick в Москве и Санкт-Петербурге</p>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- Bike Block -->
    <?php include('inc/bike-block.php') ?>
    <!-- -->

    <section class="section-dealer-three">
        <div class="container">
            <div class="h1 text-center">Качество контролируется на каждом этапе сборки, отсюда и результат:</div>

            <div class="refund">
                <ul class="refund-image clearfix">
                    <li>
                        <img src="img/refund_01.png" alt="" class="img-responsive">
                    </li>
                    <li>
                        <img src="img/refund_02.png" alt="" class="img-responsive">
                    </li>
                    <li>
                        <img src="img/refund_03.png" alt="" class="img-responsive">
                    </li>
                </ul>
                <ul class="refund-text clearfix">
                    <li>6% - число рекламаций на китайские велосипеды,которые не проходят должный контроль качества на производстве</li>
                    <li>0,3-0,5% - среднее число рекламаций по рынку.</li>
                    <li>0,1% - число рекламаций по велосипедам Maverick за 3 года.</li>
                </ul>
            </div>

        </div>
    </section>

    <!-- Скачать Каталог -->
    <?php include('inc/index-box-two.php') ?>
    <!-- -->

    <section class="section-dealer-four">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-6">
                    <div class="inner">
                        <div class="h2">Экономия сил и времени для вас</div>
                        <p>Сколько времени занимает настройка велосипеда у сотрудника вашего магазина сегодня?В среднем около 20 минут. Но те,кто продает велосипеды Maverick,не теряют это время!</p>
                        <div class="h2">Почему?</div>
                        <p>Наши велосипеды проходят настройку тормозови трансмиссии сразу на заводе. Все, что остается вам — лишь установить оборудование,а это 10 минут, и велосипед готов.Все настройки мы сделали за вас!</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-dealer-five">
        <div class="container">
            <div class="h1 text-center">Никакого риска</div>
            <div class="risk">

                <div class="risk-item">
                    <div class="risk-number">1</div>
                    <div class="risk-title">«низкий старт»</div>
                    <p>минимальная партия для заказа — всего 100 000 рублей. Как показывает практика, в 99% случаев они продаются уже в первую неделю сезона.</p>
                </div>

                <div class="risk-item">
                    <div class="risk-number">2</div>
                    <div class="risk-title">возврат</div>
                    <p>для первичного изучения спроса вы получаете демонстрационную витрину из 10 велосипедов Maverick. При этом непроданные велосипеды, если они находятся в заводской упаковке, мы готовы принять обратно на склад*.</p>
                    <small>(*условия предоставления демо-витрины обсуждаются индивидуально.)</small>
                </div>

                <div class="risk-item">
                    <div class="risk-number">3</div>
                    <div class="risk-title">гибкость</div>
                    <p>вы получаете выгодные условия сотрудничества и оплаты с учетом ваших интересов и возможностей.</p>
                </div>

            </div>
        </div>
    </section>

    <section class="section-dealer-six">
        <div class="container">
            <div class="h1 text-center">Почему мы гарантируем отличные продажи «Maverick»</div>
            <div class="who">

                <div class="who-item">
                    <div class="who-image">
                        <img src="images/dealer_img_01.jpg" alt="" class="img-responsive">
                    </div>
                    <p>Яркий и уникальный дизайн</p>
                </div>

                <div class="who-item">
                    <div class="who-image">
                        <img src="images/dealer_img_02.jpg" alt="" class="img-responsive">
                    </div>
                    <p>Надежность, практичностьи лучшее соотношение цена/качество</p>
                </div>

                <div class="who-item">
                    <div class="who-image">
                        <img src="images/dealer_img_03.jpg" alt="" class="img-responsive">
                    </div>
                    <p>Заводская настройка по всем техническим стандартам</p>
                </div>

                <div class="who-item">
                    <div class="who-image">
                        <img src="images/dealer_img_04.jpg" alt="" class="img-responsive">
                    </div>
                    <p>Честная гарантия и никаких проблем в эксплуатации</p>
                </div>
            </div>
        </div>
    </section>

    <section class="section-dealer-seven">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 hidden-xs">
                    <img src="img/dealers-map.png" alt="" class="img-responsive">
                </div>
                <div class="col-sm-6">
                    <div class="h2">Почему нам доверяют?</div>
                    <p class="margin-sm">
                        С одной стороны, на рынке бренд Maverick не так давно — с 2013 года.Но с другой, основатели компании — фанаты велосипедов и всего, что с ними связано. За несколько лет компания совершила качественный рывок, на который у других производителей ушло десятилетие.
                        При этом многие из них все еще не могут предложить уровень качества продукции, который для Maverickявляется стандартом.
                    </p>
                    <p>Качество велосипедов Maverick и надежность подтвердят более 50 партнеров в <b>Москве, Санкт-Петербурге, Нижнем Новгороде, Оренбурге, Ростове, Краснодаре</b> и других городах</p>
                </div>
            </div>
        </div>
    </section>

    <section class="section-dealer-eight">
        <div class="container">
            <div class="h1 text-center">Оставьте заявку сейчас и получите:</div>
            <ul class="rows clearfix">
                <li>
                    <ul class="list-check">

                        <li>Оптовый прайс-лист</li>
                        <li>Каталог велосипедов «Maverick» 2016</li>
                        <li>Подробную консультацию по всем вопросам сотрудничества!</li>
                        <li>2-стеночные обода собственного производства на каждом велосипеде</li>
                    </ul>
                    <div class="text-box">
                        Внимание! Уже в мае планируется повышение цен на 5%.<br/>
                        Торопитесь!
                    </div>
                </li>
                <li>
                    <img src="img/dealer-order.png" alt="" class="img-responsive">
                </li>
            </ul>


            <div class="hidden-xs">
                <form class="form ">
                    <ul class="form-row clearfix">
                        <li>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="Ваше имя">
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="Название компании">
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="Телефон">
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="email">
                            </div>
                        </li>
                        <li>
                            <button type="submit" class="btn btn-md btn-send">Получить прайс и каталог</button>
                        </li>
                    </ul>
                </form>
            </div>
            <div class="text-center hidden-sm hidden-md hidden-lg">
                <a href="#dealers-form-modal" class="btn btn-md btn-modal">Получить прайс и каталог</a>
            </div>

        </div>
    </section>

    <!-- Footer -->
    <?php include('inc/footer.php') ?>
    <!-- -->

    <!-- Modal -->
    <div class="hide">
        <div class="modal modal-xs" id="dealers-form-modal">
            <div class="modal-header">получить прайс<br/>и каталог</div>
            <div class="modal-body">
                <h3>Заявка партнёра</h3>
                <form class="form">
                    <div class="form-group">
                        <input type="text" name="" class="form-control" placeholder="Ваше имя">
                    </div>
                    <div class="form-group">
                        <input type="text" name="" class="form-control" placeholder="Название компании">
                    </div>
                    <div class="form-group">
                        <input type="text" name="" class="form-control" placeholder="Телефон">
                    </div>
                    <div class="form-group">
                        <input type="text" name="" class="form-control" placeholder="email">
                    </div>
                    <button type="submit" class="btn btn-md btn-send">Получить прайс и каталог</button>
                </form>
            </div>
        </div>
    </div>
    <!-- -->


    <!-- Scripts -->
    <?php include('inc/script.php') ?>
    <!-- -->

    </body>
</html>



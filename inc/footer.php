<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <ul class="footer-first">
                    <li>
                        <a href="#" class="footer-logo">
                            <img src="img/logo.svg" alt="">
                        </a>
                    </li>
                    <li class="footer-copy">Все права защищены ©2016</li>
                    <li class="footer-contact">г. Москва, <a href="tel:+7 (925) 118-30-70">+7 (925) 118-30-70</a></li>
                    <li class="footer-mail"><a href="mailto:veloart@bk.ru">veloart@bk.ru</a></li>
                    <li class="footer-slogan">Велосипеды оптом<br/>напрямую от производителя</li>
                    <li>
                        <ul class="footer-social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-vk"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-sm-7 hidden-xs">
                <div class="row">
                    <div class="col-sm-4">
                        <ul class="footer-nav">
                            <li><a href="#">Велосипеды</a></li>
                            <li><a href="#">Горные хардтейлы</a></li>
                            <li><a href="#">Горные двухподвесы</a></li>
                            <li><a href="#">Дорожные</a></li>
                            <li><a href="#">Подростковые</a></li>
                            <li><a href="#">Каталог 2016</a></li>
                            <li><a href="#">Каталог 2015</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <ul class="footer-nav">
                            <li><a href="#">О компании</a></li>
                            <li><a href="#">О нас</a></li>
                            <li><a href="#">Развитие компании</a></li>
                            <li><a href="#">Наша миссия</a></li>
                            <li><a href="#">Преимущества</a></li>
                            <li><a href="#">Наши партнеры</a></li>
                            <li><a href="#">Новости</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <ul class="footer-nav">
                            <li><a href="#">Где купить</a></li>
                            <li><a href="#">Сделать ТО</a></li>
                            <li><a href="#">Гарантия</a></li>
                        </ul>
                        <h4></h4>
                        <ul class="footer-nav">
                            <li><a href="#">Сотрудничество</a></li>
                            <li><a href="#">Как нас найти</a></li>
                            <li><a href="#">Контакты</a></li>
                            <li><a href="#">Вход дилерам</a></li>
                            <li><a href="#"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>